<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
	header("location:../../login");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/dataaset-superadmin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Data Aset Wesel - Super Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-user.png" alt="" class="icon">User</a>
		<a href="#setting" data-bs-toggle="collapse"><img src="../../src/icon/icon-setting.png" alt="" class="icon">Setting</a>
		<div class="collapse sub-menu" id="setting">
			<a href="../setting/"><img src="../../src/icon/icon-home.png" alt="" class="icon">Setting Home</a>
		</div>
		<a href="#resort" data-bs-toggle="collapse" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Resor</a>
		<div class="collapse sub-menu" id="resort">
			<a href="../data-resort/"><img src="../../src/icon/icon-dataresort.png" alt="" class="icon">Data Resor</a>
			<a href="../emplasemen/"><img src="../../src/icon/icon-emplasemen.png" alt="" class="icon">Emplasemen</a>
			<a href="./" class="active"><img src="../../src/icon/icon-dataaset.png" alt="" class="icon">Data Aset Wesel</a>
		</div>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="tambah-user">
			<button type="button" class="btn btn-primary btn-sm shadow-sm" data-bs-toggle="modal" data-bs-target="#tambahUser">Tambah Data Aset Wesel</button>

			<div class="modal fade" id="tambahUser" tabindex="-1" aria-labelledby="tambahUserLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tambahUserLabel">Tambah Data Aset Wesel</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<form action="confDataAset.php" enctype="multipart/form-data" method="post">
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">No Wesel</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan No Wesel" name="noWesel" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Emplasemen</label>
									<select class="form-select" aria-label="Default select example" name="emplasemen" required>
										<option selected disabled value="">Pilih Emplasemen</option>
										<?php
										$sqlSelect = "SELECT * FROM tbl_emplasemen";
										$querySelect = mysqli_query($db, $sqlSelect);
										while ($dataSelect = mysqli_fetch_array($querySelect)) {
										?>
											<option value="<?= $dataSelect['namaEmplasemen'] ?>"><?= $dataSelect['namaEmplasemen'] ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Posisi Ujung Wesel</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Posisi Ujung Wesel" name="posisiUjung" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Posisi Pangkal Wesel</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Posisi Pangkal Wesel" name="posisiPangkal" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Sudut Wesel</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Sudut Wesel" name="sudutWesel" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Merk Wesel</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Merk Wesel" name="merk" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Arah Wesel</label>
									<select class="form-select" aria-label="Default select example" name="arah" required>
										<option selected disabled value="">Pilih Arah Wesel</option>
										<option value="Kanan">Kanan</option>
										<option value="Kiri">Kiri</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Tipe Rel</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Tipe Rel" name="tipeRel" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Lidah</label>
									<select class="form-select" aria-label="Default select example" name="lidah" required>
										<option selected disabled value="">Pilih Lidah</option>
										<option value="Pegas">Pegas</option>
										<option value="Putar">Putar</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Terlayan</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Terlayan" name="terlayan" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Tahun Produksi</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Tahun Produksi" name="tahun" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Jenis Jalur</label>
									<select class="form-select" aria-label="Default select example" name="jenisJalur" required>
										<option selected disabled value="">Pilih Jenis Jalur</option>
										<option value="KA">KA</option>
										<option value="Raya">Raya</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="formFile" class="form-label">Foto Wesel ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="fotoWesel" required>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
									<button class="btn btn-primary" type="submit" value="save" name="save">Tambah</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tabel-dataAset table-responsive">
			<table class="table table-hover table-light rounded-3 overflow-hidden w-auto small" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th scope="col" class="text-center align-middle">No</th>
						<th scope="col" class="align-middle">No Wesel</th>
						<th scope="col" class="align-middle">Emplasemen</th>
						<th scope="col" class="align-middle">Posisi Ujung Wesel</th>
						<th scope="col" class="align-middle">Posisi Pangkal Wesel</th>
						<th scope="col" class="align-middle">Sudut Wesel</th>
						<th scope="col" class="align-middle">Merk Wesel</th>
						<th scope="col" class="align-middle">Arah Wesel</th>
						<th scope="col" class="align-middle">Tipe Rel</th>
						<th scope="col" class="align-middle">Lidah</th>
						<th scope="col" class="align-middle">Terlayan</th>
						<th scope="col" class="align-middle">Tahun Produksi</th>
						<th scope="col" class="align-middle">Jenis Jalur</th>
						<th scope="col" class="text-center align-middle">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_data_aset";
					$query = mysqli_query($db, $sql);
					$no = 1;

					while ($data = mysqli_fetch_array($query)) {
						echo "<tr>";

						echo "<td class='text-center'>" . $no . "</td>";
						echo "<td>" . $data['noWesel'] . "</td>";
						echo "<td>" . $data['emplasemen'] . "</td>";
						echo "<td>" . $data['posisiUjung'] . "</td>";
						echo "<td>" . $data['posisiPangkal'] . "</td>";
						echo "<td>" . $data['sudutWesel'] . "</td>";
						echo "<td>" . $data['merk'] . "</td>";
						echo "<td>" . $data['arah'] . "</td>";
						echo "<td>" . $data['tipeRel'] . "</td>";
						echo "<td>" . $data['lidah'] . "</td>";
						echo "<td>" . $data['terlayan'] . "</td>";
						echo "<td>" . $data['tahun'] . "</td>";
						echo "<td>" . $data['jenisJalur'] . "</td>";

					?>
						<td class='align-middle'>
							<div class='d-flex justify-content-center'>
								<button type='button' class='btn btn-primary btn-sm shadow-sm' data-bs-toggle='modal' data-bs-target='#editModal<?php echo $no ?>'>Edit</button>&nbsp
								<div class='modal fade' id='editModal<?php echo $no ?>' tabindex='-1' aria-labelledby='editModalLabel' aria-hidden='true'>
									<div class='modal-dialog modal-lg'>
										<div class='modal-content'>
											<div class='modal-header'>
												<h5 class='modal-title' id='editModalLabel'>Edit</h5>
												<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
											</div>
											<div class='modal-body'>
												<form action='confDataAset.php' enctype="multipart/form-data" method='post'>
													<input type='text' class='form-control' id='recipient-name' name='id' value="<?php echo $data['id'] ?>" readonly style='display:none;'>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>No Wesel</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan No Wesel' name='noWesel' value="<?php echo $data['noWesel'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Emplasemen</label>
														<select class="form-select" aria-label="Default select example" name="emplasemen" required>
															<option value="<?php echo $data['emplasemen'] ?>" selected><?php echo $data['emplasemen'] ?></option>
															<?php
															$sqlSelect2 = "SELECT * FROM tbl_emplasemen";
															$querySelect2 = mysqli_query($db, $sqlSelect2);
															while ($dataSelect2 = mysqli_fetch_array($querySelect2)) {
															?>
																<option value="<?= $dataSelect2['namaEmplasemen'] ?>"><?= $dataSelect2['namaEmplasemen'] ?></option>
															<?php
															}
															?>
														</select>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Posisi Ujung Wesel</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Posisi Ujung Wesel' name='posisiUjung' value="<?php echo $data['posisiUjung'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Posisi Pangkal Wesel</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Posisi Pangkal Wesel' name='posisiPangkal' value="<?php echo $data['posisiPangkal'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Sudut Wesel</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Sudut Wesel' name='sudutWesel' value="<?php echo $data['sudutWesel'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Merk Wesel</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Merk Wesel' name='merk' value="<?php echo $data['merk'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Arah Wesel</label>
														<select class='form-select' aria-label='Default select example' name='arah' required>
															<option selected value="<?php echo $data['arah'] ?>"><?php echo $data['arah'] ?></option>
															<option value='Kanan'>Kanan</option>
															<option value='Kiri'>Kiri</option>
														</select>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Tipe Rel</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Tipe Rel' name='tipeRel' value="<?php echo $data['tipeRel'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Lidah</label>
														<select class='form-select' aria-label='Default select example' name='lidah' required>
															<option selected value='<?php echo $data['lidah'] ?>'><?php echo $data['lidah'] ?></option>
															<option value='Pegas'>Pegas</option>
															<option value='Putar'>Putar</option>
														</select>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Terlayan</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Terlayan' name='terlayan' value="<?php echo $data['terlayan'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Tahun Produksi</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Tahun Produksi' name='tahun' value="<?php echo $data['tahun'] ?>" required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Jenis Jalur</label>
														<select class='form-select' aria-label='Default select example' name='jenisJalur' required>
															<option selected value='<?php echo $data['jenisJalur'] ?>'><?php echo $data['jenisJalur'] ?></option>
															<option value='KA'>KA</option>
															<option value='Raya'>Raya</option>
														</select>
													</div>
													<div class="mb-3">
														<label for="formFile" class="form-label">Foto Wesel ukuran max 5MB (kosongkan saja jika tidak ingin mengganti foto)</label>
														<input class="form-control" type="file" id="fotoWesel" name="fotoWesel">
													</div>
													<div class='modal-footer'>
														<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
														<button class='btn btn-primary' type='submit' value='edit' name='edit'>Simpan</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								<button type='button' class='btn btn-danger btn-sm shadow-sm' data-bs-toggle='modal' data-bs-target='#hapusModal<?php echo $no ?>'>Hapus</button>
								<div class='modal fade' id='hapusModal<?php echo $no ?>' tabindex='-1' aria-labelledby='hapusModalLabel' aria-hidden='true'>
									<div class='modal-dialog'>
										<div class='modal-content'>
											<div class='modal-header'>
												<h5 class='modal-title' id='hapusModalLabel'>Hapus</h5>
												<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
											</div>
											<form action='confDataAset.php' method='post'>
												<div class='modal-body'>
													Apakah anda yakin ingin menghapus data ini?
													<input type='text' class='form-control' id='recipient-name' name='id' value="<?php echo $data['id'] ?>" readonly style='display:none;'>
												</div>
												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
													<button type='submit' class='btn btn-danger' value='hapus' name='hapus'>Hapus</button>
											</form>
										</div>
									</div>
								</div>
							</div>
		</div>
		</td>

		</tr>
	<?php
						$no++;
					}
	?>
	</tbody>
	</table>
	</div>
	</div>
	<!-- end content -->
</body>
<script src=" https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>