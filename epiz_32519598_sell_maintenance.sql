-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql309.byetcluster.com
-- Waktu pembuatan: 08 Mar 2023 pada 12.43
-- Versi server: 10.3.27-MariaDB
-- Versi PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_32519598_sell_maintenance`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `doc_perawatan`
--

CREATE TABLE `doc_perawatan` (
  `id` varchar(100) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  `resort` varchar(244) NOT NULL,
  `emplasemen` varchar(244) NOT NULL,
  `noWesel` varchar(100) NOT NULL,
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `doc_perawatan`
--

INSERT INTO `doc_perawatan` (`id`, `tanggal`, `resort`, `emplasemen`, `noWesel`, `namaFile`) VALUES
('12nzhe78kdk7', '2023-02-09', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'NO.1031A-(09022023-12nzhe78kdk7).pdf'),
('2g5nuafovyjq', '2023-03-08', 'UPT RESOR JR 5.5 PWT', 'Purwokerto', '42', '42-(09032023-2g5nuafovyjq).pdf'),
('4n1vw99l4p13', '2023-03-08', 'UPT RESOR JR 5.5 PWT', 'Purwokerto', '1031A', '1031A-(09032023-4n1vw99l4p13).pdf'),
('7dsniighuwct', '2022-12-18', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'NO.1031A-(18122022-7dsniighuwct).pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `home`
--

CREATE TABLE `home` (
  `id` int(20) NOT NULL,
  `deskripsi` text NOT NULL,
  `strukturOrg` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `home`
--

INSERT INTO `home` (`id`, `deskripsi`, `strukturOrg`) VALUES
(1, 'Unit Jalan rel dan jembatan adalah satuan organisasi daop/divre yang dipimpin oleh Manager yang bertanggung jawab langsung kepada EVP Daop, sesuai keputusan  Direksi No. PER.U/KO.104/II/1/KA-2021 Tentang Perubahan organisasi dan tata laksana pada bagian jalan rel dan jembatan, dan bagian sintel, telekomunikasi, dan listrik.', 'strukturOrgstruktur.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_data_aset`
--

CREATE TABLE `tbl_data_aset` (
  `id` int(20) NOT NULL,
  `emplasemen` varchar(50) NOT NULL,
  `noWesel` varchar(50) NOT NULL,
  `posisiUjung` varchar(50) NOT NULL,
  `posisiPangkal` varchar(50) NOT NULL,
  `sudutWesel` varchar(20) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `arah` varchar(50) NOT NULL,
  `tipeRel` varchar(50) NOT NULL,
  `lidah` varchar(20) NOT NULL,
  `terlayan` varchar(20) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jenisJalur` varchar(50) NOT NULL,
  `fotoWesel` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_data_aset`
--

INSERT INTO `tbl_data_aset` (`id`, `emplasemen`, `noWesel`, `posisiUjung`, `posisiPangkal`, `sudutWesel`, `merk`, `arah`, `tipeRel`, `lidah`, `terlayan`, `tahun`, `jenisJalur`, `fotoWesel`) VALUES
(12, 'Purwokerto', '1031A', '349+125', '349+158', '1:10', 'China Shanghaiguan', 'Kiri', 'R.54', 'Pegas', 'Pusat', 2008, 'Raya', '1031AWesel 1031A.jpg'),
(13, 'Purwokerto', '1011C', '349+776', '349+809', '1:12', 'Austria', 'Kiri', 'R.54', 'Pegas', 'Pusat', 2013, 'Raya', '1011CWesel 1011C.JPG'),
(14, 'Purwokerto', '42', '350+131', '350+109', '1:10', 'Belanda Monoblock', 'Kanan', 'R.42', 'Pegas', 'Setempat', 0000, 'KA', '42Wesel 42.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emplasemen`
--

CREATE TABLE `tbl_emplasemen` (
  `id` int(20) NOT NULL,
  `kodeResort` varchar(50) NOT NULL,
  `namaEmplasemen` varchar(244) NOT NULL,
  `alamatEmplasemen` text NOT NULL,
  `gambarEmplasemen` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_emplasemen`
--

INSERT INTO `tbl_emplasemen` (`id`, `kodeResort`, `namaEmplasemen`, `alamatEmplasemen`, `gambarEmplasemen`) VALUES
(16, 'JR 5.5', 'Purwokerto', 'Purwokerto', 'PurwokertoEMPLASEMEN PWT 2022_001.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan`
--

CREATE TABLE `tbl_pemeriksaan` (
  `id` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `resort` varchar(50) NOT NULL,
  `emplasemen` varchar(50) NOT NULL,
  `noWesel` varchar(50) NOT NULL,
  `nama_kupt` varchar(244) NOT NULL,
  `nipp_kupt` varchar(50) NOT NULL,
  `noHp_kupt` varchar(15) NOT NULL,
  `nama_kat` varchar(244) NOT NULL,
  `nipp_kat` varchar(50) NOT NULL,
  `noHp_kat` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'belumSelesai'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan`
--

INSERT INTO `tbl_pemeriksaan` (`id`, `tanggal`, `resort`, `emplasemen`, `noWesel`, `nama_kupt`, `nipp_kupt`, `noHp_kupt`, `nama_kat`, `nipp_kat`, `noHp_kat`, `status`) VALUES
('2g5nuafovyjq', '2023-03-09', 'UPT RESOR JR 5.5 PWT', 'Purwokerto', '42', 'Tatang', '46789', '082227816093', 'Bowo', '45678', '087791270187', 'selesai'),
('4n1vw99l4p13', '2023-03-09', 'UPT RESOR JR 5.5 PWT', 'Purwokerto', '1031A', 'Tatang', '46789', '082227816093', 'Bowo', '45678', '087791270187', 'selesai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan_ls`
--

CREATE TABLE `tbl_pemeriksaan_ls` (
  `id` varchar(30) NOT NULL,
  `ls_ls6m_jarumLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls6m_jarumLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls6m_jarumLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls6m_jarumBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls6m_jarumBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls6m_jarumBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_lst_jarumLurus_nStandar` varchar(20) NOT NULL,
  `ls_lst_jarumLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_lst_jarumLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_lst_jarumBelok_nStandar` varchar(20) NOT NULL,
  `ls_lst_jarumBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_lst_jarumBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar1` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan1` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan1` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar2` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan2` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan2` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar3` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan3` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan3` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar4` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan4` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan4` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar1` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan1` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan1` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar2` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan2` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan2` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar3` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan3` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan3` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar4` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan4` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan4` varchar(20) NOT NULL,
  `ls_ls_tMatematisLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls_tMatematisLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_tMatematisLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_tMatematisBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls_tMatematisBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_tMatematisBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_pLidahLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls_pLidahLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_pLidahLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_pLidahBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls_pLidahBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_pLidahBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_lantak_nStandar` varchar(20) NOT NULL,
  `ls_ls_lantak_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_lantak_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_ppLidahLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls_ppLidahLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_ppLidahLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_ppLidahBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls_ppLidahBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_ppLidahBelok_nPerawatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan_ls`
--

INSERT INTO `tbl_pemeriksaan_ls` (`id`, `ls_ls6m_jarumLurus_nStandar`, `ls_ls6m_jarumLurus_nPemeriksaan`, `ls_ls6m_jarumLurus_nPerawatan`, `ls_ls6m_jarumBelok_nStandar`, `ls_ls6m_jarumBelok_nPemeriksaan`, `ls_ls6m_jarumBelok_nPerawatan`, `ls_lst_jarumLurus_nStandar`, `ls_lst_jarumLurus_nPemeriksaan`, `ls_lst_jarumLurus_nPerawatan`, `ls_lst_jarumBelok_nStandar`, `ls_lst_jarumBelok_nPemeriksaan`, `ls_lst_jarumBelok_nPerawatan`, `ls_la_paksaLurus_nStandar1`, `ls_la_paksaLurus_nPemeriksaan1`, `ls_la_paksaLurus_nPerawatan1`, `ls_la_paksaLurus_nStandar2`, `ls_la_paksaLurus_nPemeriksaan2`, `ls_la_paksaLurus_nPerawatan2`, `ls_la_paksaLurus_nStandar3`, `ls_la_paksaLurus_nPemeriksaan3`, `ls_la_paksaLurus_nPerawatan3`, `ls_la_paksaLurus_nStandar4`, `ls_la_paksaLurus_nPemeriksaan4`, `ls_la_paksaLurus_nPerawatan4`, `ls_la_paksaBelok_nStandar1`, `ls_la_paksaBelok_nPemeriksaan1`, `ls_la_paksaBelok_nPerawatan1`, `ls_la_paksaBelok_nStandar2`, `ls_la_paksaBelok_nPemeriksaan2`, `ls_la_paksaBelok_nPerawatan2`, `ls_la_paksaBelok_nStandar3`, `ls_la_paksaBelok_nPemeriksaan3`, `ls_la_paksaBelok_nPerawatan3`, `ls_la_paksaBelok_nStandar4`, `ls_la_paksaBelok_nPemeriksaan4`, `ls_la_paksaBelok_nPerawatan4`, `ls_ls_tMatematisLurus_nStandar`, `ls_ls_tMatematisLurus_nPemeriksaan`, `ls_ls_tMatematisLurus_nPerawatan`, `ls_ls_tMatematisBelok_nStandar`, `ls_ls_tMatematisBelok_nPemeriksaan`, `ls_ls_tMatematisBelok_nPerawatan`, `ls_ls_pLidahLurus_nStandar`, `ls_ls_pLidahLurus_nPemeriksaan`, `ls_ls_pLidahLurus_nPerawatan`, `ls_ls_pLidahBelok_nStandar`, `ls_ls_pLidahBelok_nPemeriksaan`, `ls_ls_pLidahBelok_nPerawatan`, `ls_ls_lantak_nStandar`, `ls_ls_lantak_nPemeriksaan`, `ls_ls_lantak_nPerawatan`, `ls_ls_ppLidahLurus_nStandar`, `ls_ls_ppLidahLurus_nPemeriksaan`, `ls_ls_ppLidahLurus_nPerawatan`, `ls_ls_ppLidahBelok_nStandar`, `ls_ls_ppLidahBelok_nPemeriksaan`, `ls_ls_ppLidahBelok_nPerawatan`) VALUES
('2g5nuafovyjq', '1067', '1066', '1067', '1067', '1065', '1067', '1067', '1068', '1067', '1067', '1066', '1067', '80', '79', '80', '65', '66', '65', '80', '83', '80', '65', '60', '65', '80', '78', '80', '65', '64', '65', '80', '81', '80', '65', '63', '65', '1067', '1064', '1067', '1072', '1070', '1072', '1072', '1070', '1072', '1072', '1070', '1072', '1067', '1066', '1067', '1067', '1066', '1067', '1072', '1071', '1072'),
('4n1vw99l4p13', '1067', '1066', '1067', '1067', '1065', '1067', '1067', '1068', '1067', '1067', '1066', '1067', '80', '79', '80', '65', '66', '65', '80', '83', '80', '65', '60', '65', '80', '78', '80', '65', '64', '65', '80', '81', '80', '65', '63', '65', '1067', '1064', '1067', '1072', '1070', '1072', '1072', '1070', '1072', '1072', '1070', '1072', '1067', '1066', '1067', '1067', '1066', '1067', '1072', '1071', '1072');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan_op`
--

CREATE TABLE `tbl_pemeriksaan_op` (
  `id` varchar(30) NOT NULL,
  `op_kondisiJarum` text NOT NULL,
  `op_kondisiJarum_foto` text NOT NULL,
  `op_kondisiVangrel` text NOT NULL,
  `op_kondisiVangrel_foto` text NOT NULL,
  `op_kondisiLidah` text NOT NULL,
  `op_kondisiLidah_foto` text NOT NULL,
  `op_kondisiRelLantak` text NOT NULL,
  `op_kondisiRelLantak_foto` text NOT NULL,
  `op_kondisiBantalan` text NOT NULL,
  `op_kondisiBantalan_foto` text NOT NULL,
  `op_kondisiPenambat` text NOT NULL,
  `op_kondisiPenambat_foto` text NOT NULL,
  `op_hasilJarum` varchar(20) NOT NULL,
  `op_hasilJarum_foto` text NOT NULL,
  `op_hasilVangrel` varchar(20) NOT NULL,
  `op_hasilVangrel_foto` text NOT NULL,
  `op_hasilLidah` varchar(20) NOT NULL,
  `op_hasilLidah_foto` text NOT NULL,
  `op_hasilRelLantak` varchar(20) NOT NULL,
  `op_hasilRelLantak_foto` text NOT NULL,
  `op_hasilBantalan` varchar(20) NOT NULL,
  `op_hasilBantalan_foto` text NOT NULL,
  `op_hasilPenambat` varchar(20) NOT NULL,
  `op_hasilPenambat_foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan_op`
--

INSERT INTO `tbl_pemeriksaan_op` (`id`, `op_kondisiJarum`, `op_kondisiJarum_foto`, `op_kondisiVangrel`, `op_kondisiVangrel_foto`, `op_kondisiLidah`, `op_kondisiLidah_foto`, `op_kondisiRelLantak`, `op_kondisiRelLantak_foto`, `op_kondisiBantalan`, `op_kondisiBantalan_foto`, `op_kondisiPenambat`, `op_kondisiPenambat_foto`, `op_hasilJarum`, `op_hasilJarum_foto`, `op_hasilVangrel`, `op_hasilVangrel_foto`, `op_hasilLidah`, `op_hasilLidah_foto`, `op_hasilRelLantak`, `op_hasilRelLantak_foto`, `op_hasilBantalan`, `op_hasilBantalan_foto`, `op_hasilPenambat`, `op_hasilPenambat_foto`) VALUES
('2g5nuafovyjq', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Rusak Ringan', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Rusak', '2g5nuafovyjqWesel 42.jpg', 'Rusak', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg', 'Baik', '2g5nuafovyjqWesel 42.jpg'),
('4n1vw99l4p13', 'Rusak Ringan', '4n1vw99l4p13Wesel 1031A.jpg', 'Rusak Berat', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Rusak', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg', 'Baik', '4n1vw99l4p13Wesel 1031A.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan_ukt`
--

CREATE TABLE `tbl_pemeriksaan_ukt` (
  `id` varchar(30) NOT NULL,
  `ukt_laj_paksaLurus_nStandar` varchar(20) NOT NULL,
  `ukt_laj_paksaLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_laj_paksaLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_laj_paksaBelok_nStandar` varchar(20) NOT NULL,
  `ukt_laj_paksaBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_laj_paksaBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_la_paksaLurus_nStandar` varchar(20) NOT NULL,
  `ukt_la_paksaLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_la_paksaLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_la_paksaBelok_nStandar` varchar(20) NOT NULL,
  `ukt_la_paksaBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_la_paksaBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_japl_lantakLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_japl_lantakLurus_nStandar` varchar(20) NOT NULL,
  `ukt_japl_lantakLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_japl_lantakBelok_nStandar` varchar(20) NOT NULL,
  `ukt_japl_lantakBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_japl_lantakBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_jault_lantakLurus_nStandar` varchar(20) NOT NULL,
  `ukt_jault_lantakLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_jault_lantakLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_jault_lantakBelok_nStandar` varchar(20) NOT NULL,
  `ukt_jault_lantakBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_jault_lantakBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_tss_lantak_nStandar` varchar(20) NOT NULL,
  `ukt_tss_lantak_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_tss_lantak_nPerawatan` varchar(20) NOT NULL,
  `ukt_connectingRod_nStandar` varchar(20) NOT NULL,
  `ukt_connectingRod_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_connectingRod_nPerawatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan_ukt`
--

INSERT INTO `tbl_pemeriksaan_ukt` (`id`, `ukt_laj_paksaLurus_nStandar`, `ukt_laj_paksaLurus_nPemeriksaan`, `ukt_laj_paksaLurus_nPerawatan`, `ukt_laj_paksaBelok_nStandar`, `ukt_laj_paksaBelok_nPemeriksaan`, `ukt_laj_paksaBelok_nPerawatan`, `ukt_la_paksaLurus_nStandar`, `ukt_la_paksaLurus_nPemeriksaan`, `ukt_la_paksaLurus_nPerawatan`, `ukt_la_paksaBelok_nStandar`, `ukt_la_paksaBelok_nPemeriksaan`, `ukt_la_paksaBelok_nPerawatan`, `ukt_japl_lantakLurus_nPerawatan`, `ukt_japl_lantakLurus_nStandar`, `ukt_japl_lantakLurus_nPemeriksaan`, `ukt_japl_lantakBelok_nStandar`, `ukt_japl_lantakBelok_nPemeriksaan`, `ukt_japl_lantakBelok_nPerawatan`, `ukt_jault_lantakLurus_nStandar`, `ukt_jault_lantakLurus_nPemeriksaan`, `ukt_jault_lantakLurus_nPerawatan`, `ukt_jault_lantakBelok_nStandar`, `ukt_jault_lantakBelok_nPemeriksaan`, `ukt_jault_lantakBelok_nPerawatan`, `ukt_tss_lantak_nStandar`, `ukt_tss_lantak_nPemeriksaan`, `ukt_tss_lantak_nPerawatan`, `ukt_connectingRod_nStandar`, `ukt_connectingRod_nPemeriksaan`, `ukt_connectingRod_nPerawatan`) VALUES
('2g5nuafovyjq', '1032', '1031', '1032', '1032', '1033', '1032', '35', '34', '35', '35', '36', '35', '387', '387', '386', '394', '397', '394', '120', '123', '120', '120', '118', '120', '0', '2', '0', '2', '2', '2'),
('4n1vw99l4p13', '1032', '130', '1032', '1032', '131', '1032', '35', '34', '35', '35', '36', '35', '387', '387', '386', '394', '397', '394', '120', '123', '120', '120', '118', '120', '0', '2', '0', '2', '0', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_resort`
--

CREATE TABLE `tbl_resort` (
  `id` int(20) NOT NULL,
  `kodeResort` varchar(50) NOT NULL,
  `namaResort` varchar(100) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_resort`
--

INSERT INTO `tbl_resort` (`id`, `kodeResort`, `namaResort`, `alamat`) VALUES
(20, 'JR 5.1', 'UPT RESOR JR 5.1 SLW', 'Slawi'),
(21, 'JR 5.2', 'UPT RESOR JR 5.2 PPK', 'Prupuk'),
(22, 'JR 5.3 ', 'UPT RESOR JR 5.3 BMA', 'Bumiayu'),
(23, 'JR 5.4', 'UPT RESOR JR 5.4 LGK', 'Legok'),
(24, 'JR 5.5', 'UPT RESOR JR 5.5 PWT', 'Purwokerto'),
(25, 'JR 5.6', 'UPT RESOR JR 5.6 KBS', 'Kebasen'),
(26, 'JR 5.7', 'UPT RESOR JR 5.7 LN', 'Langen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `nipp` int(20) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `kodeResort` varchar(20) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` varchar(15) NOT NULL,
  `password` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`nipp`, `nama`, `kodeResort`, `jabatan`, `no_hp`, `email`, `role`, `password`) VALUES
(19103060, 'Renita  Fauziah Samodra', 'JR 5.5', 'KUPT', '081225069824', 'renitafauziah121@gmail.com', 'super admin', '6ad14ba9986e3615423dfca256d04e3f'),
(19103079, 'Akhmad Fauzi', 'JR 5.5', 'KAUR', '081234567890', 'akhmadfauzi@gmail.com', 'admin', '6ad14ba9986e3615423dfca256d04e3f');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `doc_perawatan`
--
ALTER TABLE `doc_perawatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_data_aset`
--
ALTER TABLE `tbl_data_aset`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_emplasemen`
--
ALTER TABLE `tbl_emplasemen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan`
--
ALTER TABLE `tbl_pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan_ls`
--
ALTER TABLE `tbl_pemeriksaan_ls`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan_op`
--
ALTER TABLE `tbl_pemeriksaan_op`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan_ukt`
--
ALTER TABLE `tbl_pemeriksaan_ukt`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_resort`
--
ALTER TABLE `tbl_resort`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`nipp`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `home`
--
ALTER TABLE `home`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_data_aset`
--
ALTER TABLE `tbl_data_aset`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tbl_emplasemen`
--
ALTER TABLE `tbl_emplasemen`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tbl_resort`
--
ALTER TABLE `tbl_resort`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
