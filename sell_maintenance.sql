-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Apr 2023 pada 11.03
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sell_maintenance`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `doc_perawatan`
--

CREATE TABLE `doc_perawatan` (
  `id` varchar(100) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  `resort` varchar(244) NOT NULL,
  `emplasemen` varchar(244) NOT NULL,
  `noWesel` varchar(100) NOT NULL,
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `doc_perawatan`
--

INSERT INTO `doc_perawatan` (`id`, `tanggal`, `resort`, `emplasemen`, `noWesel`, `namaFile`) VALUES
('12nzhe78kdk7', '2023-02-09', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'NO.1031A-(09022023-12nzhe78kdk7).pdf'),
('7dsniighuwct', '2022-12-18', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'NO.1031A-(18122022-7dsniighuwct).pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `home`
--

CREATE TABLE `home` (
  `id` int(20) NOT NULL,
  `deskripsi` text NOT NULL,
  `strukturOrg` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `home`
--

INSERT INTO `home` (`id`, `deskripsi`, `strukturOrg`) VALUES
(1, 'Unit Jalan rel dan jembatan adalah satuan organisasi daop/divre yang dipimpin oleh Manager yang bertanggung jawab langsung kepada EVP Daop, sesuai keputusan  Direksi No. PER.U/KO.104/II/1/KA-2021 Tentang Perubahan organisasi dan tata laksana pada bagian jalan rel dan jembatan, dan bagian sintel, telekomunikasi, dan listrik.', 'strukturOrgstruktur.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_data_aset`
--

CREATE TABLE `tbl_data_aset` (
  `id` int(20) NOT NULL,
  `emplasemen` varchar(50) NOT NULL,
  `noWesel` varchar(50) NOT NULL,
  `posisiUjung` varchar(50) NOT NULL,
  `posisiPangkal` varchar(50) NOT NULL,
  `sudutWesel` varchar(20) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `arah` varchar(50) NOT NULL,
  `tipeRel` varchar(50) NOT NULL,
  `lidah` varchar(20) NOT NULL,
  `terlayan` varchar(20) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jenisJalur` varchar(50) NOT NULL,
  `fotoWesel` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_data_aset`
--

INSERT INTO `tbl_data_aset` (`id`, `emplasemen`, `noWesel`, `posisiUjung`, `posisiPangkal`, `sudutWesel`, `merk`, `arah`, `tipeRel`, `lidah`, `terlayan`, `tahun`, `jenisJalur`, `fotoWesel`) VALUES
(8, 'Emplasemen Purwokerto', 'NO.1031A', '349 + 125', '349 + 158', '1:10', 'China Shanghaiguan', 'Kiri', 'R.54', 'Pegas', 'Pusat', 2008, 'KA', 'NO.1031Awesel.jpg'),
(9, 'Emplasemen Purwokerto', 'NO.1031B', '349 + 719', '349 + 752', '1:10', 'China Shanghaiguan', 'Kanan', 'R.54', 'Pegas', 'PUSAT', 2008, 'KA', 'NO.1031Bwesel.jpg'),
(11, 'Emplasemen Purwokerto', 'NO.1031D', '349 + 806', '349 + 831', '1:10', 'China Shanghaiguan', 'Kanan', 'R.54', 'Pegas', 'Pusat', 2018, 'KA', 'NO.1031Dwesel.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emplasemen`
--

CREATE TABLE `tbl_emplasemen` (
  `id` int(20) NOT NULL,
  `kodeResort` varchar(50) NOT NULL,
  `namaEmplasemen` varchar(244) NOT NULL,
  `alamatEmplasemen` text NOT NULL,
  `gambarEmplasemen` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_emplasemen`
--

INSERT INTO `tbl_emplasemen` (`id`, `kodeResort`, `namaEmplasemen`, `alamatEmplasemen`, `gambarEmplasemen`) VALUES
(11, 'JR 5.5', 'Emplasemen Karanggandul', 'Karanggandul', 'Emplasemen Karanggandulemplasemen-pwt.jpg'),
(13, 'JR 5.5', 'Emplasemen Purwokerto', 'Purwokerto', 'Emplasemen Purwokertoemplasemen-pwt.jpg'),
(15, 'JR 5.1', 'Stasiun Slawi', 'Slawi', 'Stasiun Slawi187697.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan`
--

CREATE TABLE `tbl_pemeriksaan` (
  `id` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `resort` varchar(50) NOT NULL,
  `emplasemen` varchar(50) NOT NULL,
  `noWesel` varchar(50) NOT NULL,
  `nama_kupt` varchar(244) NOT NULL,
  `nipp_kupt` varchar(50) NOT NULL,
  `noHp_kupt` varchar(15) NOT NULL,
  `nama_kat` varchar(244) NOT NULL,
  `nipp_kat` varchar(50) NOT NULL,
  `noHp_kat` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'belumSelesai'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan`
--

INSERT INTO `tbl_pemeriksaan` (`id`, `tanggal`, `resort`, `emplasemen`, `noWesel`, `nama_kupt`, `nipp_kupt`, `noHp_kupt`, `nama_kat`, `nipp_kat`, `noHp_kat`, `status`) VALUES
('12nzhe78kdk7', '2023-02-09', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'k', 'k', 'k', 'k', 'k', 'k', 'selesai'),
('164abfqoyhhk', '2023-03-28', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031D', '', '', '', '', '', '', 'belumSelesai'),
('3o6g7j2x95us', '2022-12-22', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'XIXIXI', '0987', '567656765', 'XAXAXA', '7890', '656765676', 'selesai'),
('7dsniighuwct', '2022-10-14', 'UPT RESOR JR 5.5 PWT', 'Emplasemen Purwokerto', 'NO.1031A', 'SI A', 'NIPP SI A', 'NO HP SI A', 'SI B ', 'NIPP SI B', 'NO HP SI B', 'selesai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan_ls`
--

CREATE TABLE `tbl_pemeriksaan_ls` (
  `id` varchar(30) NOT NULL,
  `ls_ls6m_jarumLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls6m_jarumLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls6m_jarumLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls6m_jarumBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls6m_jarumBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls6m_jarumBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_lst_jarumLurus_nStandar` varchar(20) NOT NULL,
  `ls_lst_jarumLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_lst_jarumLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_lst_jarumBelok_nStandar` varchar(20) NOT NULL,
  `ls_lst_jarumBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_lst_jarumBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar1` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan1` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan1` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar2` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan2` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan2` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar3` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan3` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan3` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nStandar4` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPemeriksaan4` varchar(20) NOT NULL,
  `ls_la_paksaLurus_nPerawatan4` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar1` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan1` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan1` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar2` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan2` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan2` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar3` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan3` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan3` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nStandar4` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPemeriksaan4` varchar(20) NOT NULL,
  `ls_la_paksaBelok_nPerawatan4` varchar(20) NOT NULL,
  `ls_ls_tMatematisLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls_tMatematisLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_tMatematisLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_tMatematisBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls_tMatematisBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_tMatematisBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_pLidahLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls_pLidahLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_pLidahLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_pLidahBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls_pLidahBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_pLidahBelok_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_lantak_nStandar` varchar(20) NOT NULL,
  `ls_ls_lantak_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_lantak_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_ppLidahLurus_nStandar` varchar(20) NOT NULL,
  `ls_ls_ppLidahLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_ppLidahLurus_nPerawatan` varchar(20) NOT NULL,
  `ls_ls_ppLidahBelok_nStandar` varchar(20) NOT NULL,
  `ls_ls_ppLidahBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ls_ls_ppLidahBelok_nPerawatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan_ls`
--

INSERT INTO `tbl_pemeriksaan_ls` (`id`, `ls_ls6m_jarumLurus_nStandar`, `ls_ls6m_jarumLurus_nPemeriksaan`, `ls_ls6m_jarumLurus_nPerawatan`, `ls_ls6m_jarumBelok_nStandar`, `ls_ls6m_jarumBelok_nPemeriksaan`, `ls_ls6m_jarumBelok_nPerawatan`, `ls_lst_jarumLurus_nStandar`, `ls_lst_jarumLurus_nPemeriksaan`, `ls_lst_jarumLurus_nPerawatan`, `ls_lst_jarumBelok_nStandar`, `ls_lst_jarumBelok_nPemeriksaan`, `ls_lst_jarumBelok_nPerawatan`, `ls_la_paksaLurus_nStandar1`, `ls_la_paksaLurus_nPemeriksaan1`, `ls_la_paksaLurus_nPerawatan1`, `ls_la_paksaLurus_nStandar2`, `ls_la_paksaLurus_nPemeriksaan2`, `ls_la_paksaLurus_nPerawatan2`, `ls_la_paksaLurus_nStandar3`, `ls_la_paksaLurus_nPemeriksaan3`, `ls_la_paksaLurus_nPerawatan3`, `ls_la_paksaLurus_nStandar4`, `ls_la_paksaLurus_nPemeriksaan4`, `ls_la_paksaLurus_nPerawatan4`, `ls_la_paksaBelok_nStandar1`, `ls_la_paksaBelok_nPemeriksaan1`, `ls_la_paksaBelok_nPerawatan1`, `ls_la_paksaBelok_nStandar2`, `ls_la_paksaBelok_nPemeriksaan2`, `ls_la_paksaBelok_nPerawatan2`, `ls_la_paksaBelok_nStandar3`, `ls_la_paksaBelok_nPemeriksaan3`, `ls_la_paksaBelok_nPerawatan3`, `ls_la_paksaBelok_nStandar4`, `ls_la_paksaBelok_nPemeriksaan4`, `ls_la_paksaBelok_nPerawatan4`, `ls_ls_tMatematisLurus_nStandar`, `ls_ls_tMatematisLurus_nPemeriksaan`, `ls_ls_tMatematisLurus_nPerawatan`, `ls_ls_tMatematisBelok_nStandar`, `ls_ls_tMatematisBelok_nPemeriksaan`, `ls_ls_tMatematisBelok_nPerawatan`, `ls_ls_pLidahLurus_nStandar`, `ls_ls_pLidahLurus_nPemeriksaan`, `ls_ls_pLidahLurus_nPerawatan`, `ls_ls_pLidahBelok_nStandar`, `ls_ls_pLidahBelok_nPemeriksaan`, `ls_ls_pLidahBelok_nPerawatan`, `ls_ls_lantak_nStandar`, `ls_ls_lantak_nPemeriksaan`, `ls_ls_lantak_nPerawatan`, `ls_ls_ppLidahLurus_nStandar`, `ls_ls_ppLidahLurus_nPemeriksaan`, `ls_ls_ppLidahLurus_nPerawatan`, `ls_ls_ppLidahBelok_nStandar`, `ls_ls_ppLidahBelok_nPemeriksaan`, `ls_ls_ppLidahBelok_nPerawatan`) VALUES
('12nzhe78kdk7', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k'),
('3o6g7j2x95us', 'A', 'B', 'AB', 'C', 'D', 'CD', 'E', 'F', 'EF', 'G', 'H', 'GH', 'I', 'J', 'IJ', 'K', 'L', 'KL', 'M', 'N', 'MN', 'O', 'P', 'OP', 'Q', 'R', 'QR', 'S', 'T', 'ST', 'U', 'V', 'UV', 'W', 'X', 'WX', 'Y', 'Z', 'YZ', 'A', 'B', 'AB', 'G', 'H', 'GH', 'I', 'J', 'IJ', 'K', 'L', 'KL', 'C', 'D', 'CD', 'E', 'F', 'EF'),
('7dsniighuwct', 'A', 'B', 'AB', 'C', 'D', 'CD', 'E', 'F', 'EF', 'G', 'H', 'GH', 'I', 'J', 'IJ', 'K', 'L', 'KL', 'M', 'N', 'MN', 'O', 'P', 'OP', 'Q', 'R', 'QR', 'S', 'T', 'ST', 'U', 'V', 'UV', 'W', 'X', 'WX', 'Y', 'Z', 'YZ', 'A', 'B', 'AB', 'G', 'H', 'GH', 'I', 'J', 'IJ', 'K', 'L', 'KL', 'C', 'D', 'CD', 'E', 'F', 'EF');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan_op`
--

CREATE TABLE `tbl_pemeriksaan_op` (
  `id` varchar(30) NOT NULL,
  `op_kondisiJarum` text NOT NULL,
  `op_kondisiJarum_foto` text NOT NULL,
  `op_kondisiVangrel` text NOT NULL,
  `op_kondisiVangrel_foto` text NOT NULL,
  `op_kondisiLidah` text NOT NULL,
  `op_kondisiLidah_foto` text NOT NULL,
  `op_kondisiRelLantak` text NOT NULL,
  `op_kondisiRelLantak_foto` text NOT NULL,
  `op_kondisiBantalan` text NOT NULL,
  `op_kondisiBantalan_foto` text NOT NULL,
  `op_kondisiPenambat` text NOT NULL,
  `op_kondisiPenambat_foto` text NOT NULL,
  `op_hasilJarum` varchar(20) NOT NULL,
  `op_hasilJarum_foto` text NOT NULL,
  `op_hasilVangrel` varchar(20) NOT NULL,
  `op_hasilVangrel_foto` text NOT NULL,
  `op_hasilLidah` varchar(20) NOT NULL,
  `op_hasilLidah_foto` text NOT NULL,
  `op_hasilRelLantak` varchar(20) NOT NULL,
  `op_hasilRelLantak_foto` text NOT NULL,
  `op_hasilBantalan` varchar(20) NOT NULL,
  `op_hasilBantalan_foto` text NOT NULL,
  `op_hasilPenambat` varchar(20) NOT NULL,
  `op_hasilPenambat_foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan_op`
--

INSERT INTO `tbl_pemeriksaan_op` (`id`, `op_kondisiJarum`, `op_kondisiJarum_foto`, `op_kondisiVangrel`, `op_kondisiVangrel_foto`, `op_kondisiLidah`, `op_kondisiLidah_foto`, `op_kondisiRelLantak`, `op_kondisiRelLantak_foto`, `op_kondisiBantalan`, `op_kondisiBantalan_foto`, `op_kondisiPenambat`, `op_kondisiPenambat_foto`, `op_hasilJarum`, `op_hasilJarum_foto`, `op_hasilVangrel`, `op_hasilVangrel_foto`, `op_hasilLidah`, `op_hasilLidah_foto`, `op_hasilRelLantak`, `op_hasilRelLantak_foto`, `op_hasilBantalan`, `op_hasilBantalan_foto`, `op_hasilPenambat`, `op_hasilPenambat_foto`) VALUES
('12nzhe78kdk7', 'k', '12nzhe78kdk7Kai Wesel-01.png', 'k', '12nzhe78kdk73940544672.jpg', 'k', '12nzhe78kdk7Remini20220803174736843.jpg', 'k', '12nzhe78kdk7debt-records.png', 'k', '12nzhe78kdk7sempro dua 2.jpg', 'k', '12nzhe78kdk7sempro dua 1.jpg', 'k', '12nzhe78kdk7907775.jpg', 'k', '12nzhe78kdk7Deskripsi Produk 2.jpg', 'k', '12nzhe78kdk7IMG_20220721_100605.jpg', 'k', '12nzhe78kdk7WhatsApp Image 2022-08-03 at 17.50.27.jpeg', 'k', '12nzhe78kdk7Group 234.png', 'k', '12nzhe78kdk7Home.png'),
('3o6g7j2x95us', 'baik', '3o6g7j2x95us187697.jpg', 'rusak', '3o6g7j2x95usFSKOC5PWUAETb9H.jpg', 'baik', '3o6g7j2x95usKai Wesel-01.png', 'rusak', '3o6g7j2x95usdebt-records.png', 'baik', '3o6g7j2x95us907775.jpg', 'rusak', '3o6g7j2x95usMTK SD.png', 'sudah di perbaiki', '3o6g7j2x95usScreenshot_2022-06-19-15-56-55-001_com.example.praktikum_pertemuan_10.jpg', 'sudah di perbaiki', '3o6g7j2x95usScreenshot_2022-06-19-15-56-47-361_com.example.praktikum_pertemuan_10.jpg', 'sudah di perbaiki', '3o6g7j2x95usScreenshot_2022-06-19-15-56-36-715_com.example.praktikum_pertemuan_10.jpg', 'sudah di perbaiki', '3o6g7j2x95usScreenshot_2022-06-19-15-56-23-346_com.example.praktikum_pertemuan_10.jpg', 'sudah di perbaiki', '3o6g7j2x95ussatu.jpg', 'sudah di perbaiki', '3o6g7j2x95ussasad.jpg'),
('7dsniighuwct', 'BAIK', '7dsniighuwct04ea90a6f35d76e8964e118640d4b2fa.jpg', 'RUSAK', '7dsniighuwct723c7be47cf91ecf264b8fd07e3705c9.jpg', 'BAIK', '7dsniighuwct6654928_7134a531-77a5-4191-a08f-c3263f3a57ee_1500_1500.jpg', 'RUSAK', '7dsniighuwct20200615113243b2f8da0cf1d7be70d09c0e922fde6ee9.jpeg', 'BAIK', '7dsniighuwct20200615161011d0bb9c837344b597049b67d0898cbf75.jpeg', 'RUSAK', '7dsniighuwct20200615193337da9cc18beb6419c05d025dc651a4d837.jpeg', 'MASIH BAIK', '7dsniighuwct20200615193337da9cc18beb6419c05d025dc651a4d837.jpeg', 'SUDAH DI PERBAIKI', '7dsniighuwct20200615194557c0f63961b549064c8aa1bcde62857d41.jpeg', 'MASIH BAIK', '7dsniighuwct202006151926106c3cd59344ca17857072130d6352701a.jpeg', 'SUDAH DI PERBAIKI', '7dsniighuwct2020061215241361da7eebd046c6406eea62a5e45b08bc.png', 'MASIH BAIK', '7dsniighuwct2020061515033808bde98184696bc73348e5d30abb6b46.jpeg', 'SUDAH DI PERBAIKI', '7dsniighuwct2020061516111745a046ee02c506aa469a828fe8cb8cdd.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pemeriksaan_ukt`
--

CREATE TABLE `tbl_pemeriksaan_ukt` (
  `id` varchar(30) NOT NULL,
  `ukt_laj_paksaLurus_nStandar` varchar(20) NOT NULL,
  `ukt_laj_paksaLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_laj_paksaLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_laj_paksaBelok_nStandar` varchar(20) NOT NULL,
  `ukt_laj_paksaBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_laj_paksaBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_la_paksaLurus_nStandar` varchar(20) NOT NULL,
  `ukt_la_paksaLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_la_paksaLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_la_paksaBelok_nStandar` varchar(20) NOT NULL,
  `ukt_la_paksaBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_la_paksaBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_japl_lantakLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_japl_lantakLurus_nStandar` varchar(20) NOT NULL,
  `ukt_japl_lantakLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_japl_lantakBelok_nStandar` varchar(20) NOT NULL,
  `ukt_japl_lantakBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_japl_lantakBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_jault_lantakLurus_nStandar` varchar(20) NOT NULL,
  `ukt_jault_lantakLurus_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_jault_lantakLurus_nPerawatan` varchar(20) NOT NULL,
  `ukt_jault_lantakBelok_nStandar` varchar(20) NOT NULL,
  `ukt_jault_lantakBelok_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_jault_lantakBelok_nPerawatan` varchar(20) NOT NULL,
  `ukt_tss_lantak_nStandar` varchar(20) NOT NULL,
  `ukt_tss_lantak_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_tss_lantak_nPerawatan` varchar(20) NOT NULL,
  `ukt_connectingRod_nStandar` varchar(20) NOT NULL,
  `ukt_connectingRod_nPemeriksaan` varchar(20) NOT NULL,
  `ukt_connectingRod_nPerawatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_pemeriksaan_ukt`
--

INSERT INTO `tbl_pemeriksaan_ukt` (`id`, `ukt_laj_paksaLurus_nStandar`, `ukt_laj_paksaLurus_nPemeriksaan`, `ukt_laj_paksaLurus_nPerawatan`, `ukt_laj_paksaBelok_nStandar`, `ukt_laj_paksaBelok_nPemeriksaan`, `ukt_laj_paksaBelok_nPerawatan`, `ukt_la_paksaLurus_nStandar`, `ukt_la_paksaLurus_nPemeriksaan`, `ukt_la_paksaLurus_nPerawatan`, `ukt_la_paksaBelok_nStandar`, `ukt_la_paksaBelok_nPemeriksaan`, `ukt_la_paksaBelok_nPerawatan`, `ukt_japl_lantakLurus_nPerawatan`, `ukt_japl_lantakLurus_nStandar`, `ukt_japl_lantakLurus_nPemeriksaan`, `ukt_japl_lantakBelok_nStandar`, `ukt_japl_lantakBelok_nPemeriksaan`, `ukt_japl_lantakBelok_nPerawatan`, `ukt_jault_lantakLurus_nStandar`, `ukt_jault_lantakLurus_nPemeriksaan`, `ukt_jault_lantakLurus_nPerawatan`, `ukt_jault_lantakBelok_nStandar`, `ukt_jault_lantakBelok_nPemeriksaan`, `ukt_jault_lantakBelok_nPerawatan`, `ukt_tss_lantak_nStandar`, `ukt_tss_lantak_nPemeriksaan`, `ukt_tss_lantak_nPerawatan`, `ukt_connectingRod_nStandar`, `ukt_connectingRod_nPemeriksaan`, `ukt_connectingRod_nPerawatan`) VALUES
('12nzhe78kdk7', 'a', 'b', 'k', 'c', 'd', 'k', 'e', 'f', 'k', 'g', 'h', 'k', 'k', 'i', 'j', 'k', 'l', 'k', 'o', 'p', 'k', 'q', 'r', 'k', 's', 't', 'k', 'm', 'n', 'k'),
('164abfqoyhhk', 'A', 'B', '', 'C', 'D', '', 'E', 'F', '', 'G', 'H', '', '', 'I', 'J', 'K', 'L', '', 'O', 'P', '', 'Q', 'R', '', 'S', 'T', '', 'M', 'N', ''),
('3o6g7j2x95us', 'A', 'B', 'AB', 'C', 'D', 'CD', 'E', 'F', 'EF', 'G', 'H', 'GH', 'IJ', 'I', 'J', 'K', 'L', 'KL', 'O', 'P', 'OP', 'Q', 'R', 'QR', 'S', 'T', 'ST', 'M', 'N', 'MN'),
('7dsniighuwct', 'A', 'B', 'AB', 'C', 'D', 'CD', 'E', 'F', 'EF', 'G', 'H', 'GH', 'IJ', 'I', 'J', 'K', 'L', 'KL', 'O', 'P', 'OP', 'Q', 'R', 'QR', 'S', 'T', 'ST', 'M', 'N', 'MN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_resort`
--

CREATE TABLE `tbl_resort` (
  `id` int(20) NOT NULL,
  `kodeResort` varchar(50) NOT NULL,
  `namaResort` varchar(100) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_resort`
--

INSERT INTO `tbl_resort` (`id`, `kodeResort`, `namaResort`, `alamat`) VALUES
(7, 'JR 5.2', 'UPT RESOR JR 5.2 PPK', 'PPK'),
(8, 'JR 5.3', 'UPT RESOR JR 5.3 BMA', 'BMA'),
(9, 'JR 5.4', 'UPT RESOR JR 5.4 LGK', 'LGK'),
(10, 'JR 5.5', 'UPT RESOR JR 5.5 PWT', 'PWT'),
(11, 'JR 5.6', 'UPT RESOR JR 5.6. KBS', 'KBS'),
(12, 'JR 5.7', 'UPT RESOR JR 5.7 LN', 'LN'),
(13, 'JR 5.8', 'UPT RESOR JR 5.8. JRL', 'JRL'),
(14, 'JR 5.9', 'UPT RESOR JR 5.9 CP', 'CP'),
(15, 'JR 5.10', 'UPT RESOR JR 5.10 KYA', 'KYA'),
(16, 'JR 5.11', 'UPT RESOR JR 5.11 TBK', 'TBK'),
(17, 'JR 5.12', 'UPT RESOR JR 5.12 GB', 'GB'),
(18, 'JR 5.13', 'UPT RESOR JR 5.13 KM', 'KM'),
(19, 'JR 5.14', 'UPT RESOR JR 5.14 KTA', 'KTA'),
(20, 'JR 5.1', 'UPT RESOR JR 5.1. SLW', 'SLW');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `nipp` int(20) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `kodeResort` varchar(20) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` varchar(15) NOT NULL,
  `password` varchar(244) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`nipp`, `nama`, `kodeResort`, `jabatan`, `no_hp`, `email`, `role`, `password`) VALUES
(22222, 'tester2', 'JR 5.5', 'tukang', '01920910291', 'tester2@gmail.com', 'super admin', '3d2172418ce305c7d16d4b05597c6a59'),
(33333, 'tester3', 'JR 5.5', 'tester3', '09249384738', 'tester3@gmail.com', 'admin', 'b7bc2a2f5bb6d521e64c8974c143e9a0'),
(53261, 'Sigit', 'JR 5.5', 'Div. Program', '081234567890', 'sigit@gmail.com', 'super admin', '80ec08504af83331911f5882349af59d'),
(90909, 'coba1', 'JR 5.5', 'bos', '081347890520', 'coba1@gmail,com', 'admin', '888e931d6360ee143df0d552f955299a'),
(123456, 'tester2', 'JR 5.5', 'tukang', '080808', 'tester2@gmail.com', 'super admin', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `doc_perawatan`
--
ALTER TABLE `doc_perawatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_data_aset`
--
ALTER TABLE `tbl_data_aset`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_emplasemen`
--
ALTER TABLE `tbl_emplasemen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan`
--
ALTER TABLE `tbl_pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan_ls`
--
ALTER TABLE `tbl_pemeriksaan_ls`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan_op`
--
ALTER TABLE `tbl_pemeriksaan_op`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pemeriksaan_ukt`
--
ALTER TABLE `tbl_pemeriksaan_ukt`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_resort`
--
ALTER TABLE `tbl_resort`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`nipp`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `home`
--
ALTER TABLE `home`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_data_aset`
--
ALTER TABLE `tbl_data_aset`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tbl_emplasemen`
--
ALTER TABLE `tbl_emplasemen`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbl_resort`
--
ALTER TABLE `tbl_resort`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
