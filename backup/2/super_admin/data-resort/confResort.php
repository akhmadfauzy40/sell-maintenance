<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
    header("location:../login");
    exit;
}

if (isset($_POST['save'])) {
    $namaResort = $_POST['namaResort'];
    $kodeResort = $_POST['kodeResort'];
    $alamat = $_POST['alamat'];

    $query = "INSERT INTO tbl_resort (kodeResort, namaResort, alamat) VALUE ('$kodeResort', '$namaResort', '$alamat')";
    $sql = mysqli_query($db, $query);

    if ($sql) {
        echo "
	        <script>
	            alert('DATA BARU BERHASIL DI INPUT');
				document.location.href = '../data-resort';
	        </script>
	    ";
    } else {
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../data-resort';
	        </script>
	    ";
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $namaResort = $_POST['namaResort'];
    $kodeResort = $_POST['kodeResort'];
    $alamat = $_POST['alamat'];

    $query = "UPDATE tbl_resort SET kodeResort='$kodeResort', namaResort='$namaResort', alamat='$alamat' WHERE id=$id";
    $sql = mysqli_query($db, $query);

    if ($sql) {
        echo "
                <script>
                    alert('DATA BERHASIL DI EDIT');
                    document.location.href = '../data-resort';
                </script>
            ";
    } else {
        echo "
                <script>
                    alert('DATA GAGAL DI EDIT');
                    document.location.href = '../data-resort';
                </script>
            ";
    }
}

if (isset($_POST['hapus'])) {
    $id = $_POST['id'];

    $pickData = "SELECT * FROM tbl_resort WHERE id='$id'";
    $SqlPickData = mysqli_query($db, $pickData);
    if ($data = mysqli_fetch_array($SqlPickData)) {
        $kodeResort = $data['kodeResort'];

        $queryChecker = "SELECT * FROM tbl_emplasemen WHERE kodeResort = '$kodeResort'";
        $sqlChecker = mysqli_query($db, $queryChecker);
        if (mysqli_num_rows($sqlChecker) != 0) {
            echo "
                <script>
                    alert('SEBELUM MENGHAPUS RESOR INI, HARAP HAPUS DULU DATA WESEL DAN DATA EMPLASEMEN YANG TERMASUK DALAM LINGKUP RESOR INI');
                    document.location.href = '../data-resort';
                </script>
            ";
        } else {
            $query = "DELETE FROM tbl_resort WHERE id='$id'";
            $sql = mysqli_query($db, $query);

            if ($sql) {
                echo "
            <script>
                alert('DATA BERHASIL DI HAPUS');
                document.location.href = '../data-resort';
            </script>
        ";
            } else {
                echo "
            <script>
                alert('DATA GAGAL DI HAPUS');
                document.location.href = '../data-resort';
            </script>
        ";
            }
        }
    }
}
