<?php
include("../../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../../login");
	exit;
}

$jalur = $_GET['jalur'];
$tahun = $_GET['tahun'];
$periode = $_GET['periode'];

if ($periode == "semester1") {
	// $sql = "SELECT * FROM tbl_data_aset LEFT JOIN tbl_pemeriksaan";
} else if ($periode == "semester2") {
} else if ($periode == "triwulan1") {
} else if ($periode == "triwulan2") {
} else if ($periode == "triwulan3") {
} else if ($periode == "triwulan4") {
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../../css/statusreportingdetailwesel-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Status Reporting Detail Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../../"><img src="../../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../../data-aset/"><img src="../../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../../arsip/"><img src="../../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../" class="active"><img src="../../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../../about/"><img src="../../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../../logout.php"><img src="../../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul">
			<?php
			if ($periode == "semester1") {
			?>
				<p>Semester I</p>
			<?php
			} else if ($periode == "semester2") {
			?>
				<p>Semester II</p>
			<?php
			} else if ($periode == "triwulan1") {
			?>
				<p>Triwulan I</p>
			<?php
			} else if ($periode == "triwulan2") {
			?>
				<p>Triwulan II</p>
			<?php
			} else if ($periode == "triwulan3") {
			?>
				<p>Triwulan III</p>
			<?php
			} else if ($periode == "triwulan4") {
			?>
				<p>Triwulan IV</p>
			<?php
			}
			?>
		</div>

		<div class="tabel table-responsive mb-4">
			<table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th scope="col" width="5%" class="text-center">No</th>
						<th scope="col" class="text-center">Resor</th>
						<th scope="col" class="text-center">Emplasemen</th>
						<th scope="col" class="text-center">Nomor Wesel</th>
						<th scope="col" class="text-center">Tanggal Pemeriksaan</th>
						<th scope="col" class="text-center">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_data_aset WHERE jenisJalur = '$jalur'";
					$query = mysqli_query($db, $sql);
					$no = 1;
					while ($data = mysqli_fetch_array($query)) {
					?>
						<tr>
							<td class='text-center'><?php echo $no ?></td>
							<?php
							$sqlResort = "SELECT * FROM tbl_emplasemen WHERE namaEmplasemen='" . $data['emplasemen'] . "'";
							$queryResort = mysqli_query($db, $sqlResort);
							$dataResort = mysqli_fetch_assoc($queryResort);
							?>
							<td class='text-center'><?php echo $dataResort['kodeResort'] ?></td>
							<td class='text-center'><?php echo $data['emplasemen'] ?></td>
							<td class='text-center'><?php echo $data['noWesel'] ?></td>
							<?php
							if ($periode == "semester1") {
								$sqlChek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='".$data['noWesel']."' AND month(tanggal)>0 AND month(tanggal)<7 AND year(tanggal)=$tahun";
							} else if ($periode == "semester2") {
								$sqlChek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='".$data['noWesel']."' AND month(tanggal)>6 AND month(tanggal)<13 AND year(tanggal)=$tahun";
							} else if ($periode == "triwulan1") {
								$sqlChek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='".$data['noWesel']."' AND month(tanggal)>0 AND month(tanggal)<4 AND year(tanggal)=$tahun";
							} else if ($periode == "triwulan2") {
								$sqlChek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='".$data['noWesel']."' AND month(tanggal)>3 AND month(tanggal)<7 AND year(tanggal)=$tahun";
							} else if ($periode == "triwulan3") {
								$sqlChek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='".$data['noWesel']."' AND month(tanggal)>6 AND month(tanggal)<10 AND year(tanggal)=$tahun";
							} else if ($periode == "triwulan4") {
								$sqlChek = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='".$data['noWesel']."' AND month(tanggal)>9 AND month(tanggal)<13 AND year(tanggal)=$tahun";
							}
							$queryChek = mysqli_query($db,$sqlChek);
							$dataChek = mysqli_fetch_array($queryChek);
							if (mysqli_num_rows($queryChek) != 0) {
							?>
								<td class='text-center'><?php echo $dataChek['tanggal'] ?></td>
							<?php
							} else {
							?>
								<td class='text-center'>-</td>
							<?php
							}
							?>
							<td class='align-middle'>
								<!-- ini tombolnya udah aku bikin 2 kondisi zi, yang complete sama incomplete -->
								<?php
								if (mysqli_num_rows($queryChek) == 0 || $dataChek['status'] != "selesai") {
								?>
									<div class='d-flex justify-content-center incomplete'>
										<button type="button" class="btn btn-primary btn-sm" disabled>Incomplete</button>
									</div>
								<?php
								} else {
								?>
									<div class='d-flex justify-content-center complete'>
										<button type="button" class="btn btn-primary btn-sm" disabled>Complete</button>
									</div>
								<?php
								}
								?>
								<!-- <div class='d-flex justify-content-center incomplete'>
								<button type="button" class="btn btn-primary btn-sm" disabled>Incomplete</button>
							</div> -->
							</td>
						</tr>
					<?php
					$no++;
					}
					?>
					<!-- <tr>
						<td class='text-center'>2</td>
						<td class='text-center'>J.R.5.5</td>
						<td class='text-center'>Emplasemen Karanggandul</td>
						<td class='text-center'>NO.1031B</td>
						<td class='text-center'>22-11-2022</td>
						<td class='align-middle'>
							ini tombolnya udah aku bikin 2 kondisi zi, yang complete sama incomplete
							<div class='d-flex justify-content-center complete'>
								<button type="button" class="btn btn-primary btn-sm" disabled>Complete</button>
							</div>
							<div class='d-flex justify-content-center incomplete'>
								<button type="button" class="btn btn-primary btn-sm" disabled>Incomplete</button>
							</div>
						</td>
					</tr> -->
				</tbody>
			</table>
		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>