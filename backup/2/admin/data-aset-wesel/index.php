<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}

$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/dataasetwesel-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Data Aset Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">Data Aset Wesel</p>
			<?php
			} else {
			?> 
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>

		<div class="gambar-emplasemen text-center">
			<?php
			$queryGambar = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlGambar = mysqli_query($db, $queryGambar);
			if ($dataGambar = mysqli_fetch_array($sqlGambar)) {
			?>
				<img src="../../src/img/<?php echo $dataGambar['gambarEmplasemen'] ?>" alt="Gambar Emplasemen">
			<?php
			}
			?>
		</div>

		<div class="search">
			<form action="" method="POST">
				<div class="input-group d-flex justify-content-end">
					<div class="form-outline">
						<input type="search" id="form1" class="form-control" placeholder="Cari" name="keyword" autocomplete="off" />
					</div>
					<button type="submit" class="btn btn-primary" value="cari" name="cari">
						<i class="fa fa-search"></i>
					</button>
				</div>
			</form>
		</div>

		<div class="konten mb-3">
			<div class="row">
				<?php
				if (isset($_POST['cari'])) {
					$keyword = $_POST['keyword'];
					$sql = "SELECT * FROM `tbl_data_aset` WHERE emplasemen='$emplasemen' AND noWesel LIKE '%$keyword%'";
				} else if (isset($_POST['cari']) && $_POST['keyword'] == "") {
					$sql = "SELECT * FROM `tbl_data_aset` WHERE emplasemen='$emplasemen'";
				} else {
					$sql = "SELECT * FROM `tbl_data_aset` WHERE emplasemen='$emplasemen'";
				}
				$query = mysqli_query($db, $sql);

				if (mysqli_num_rows($query) != 0) {
					while ($data = mysqli_fetch_array($query)) {
				?>
						<div class="col-sm-4">
							<div class="card" style="width: 100%;">
								<img src="../../src/img/<?php echo $data['fotoWesel'] ?>" class="card-img-top" alt="...">
								<div class="card-body d-flex justify-content-between">
									<p class="card-text">WESEL <?php echo $data['noWesel'] ?></p>
									<a class="btn btn-light btn-sm tombol" href="../detail-data-aset/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $data['noWesel'] ?>" role="button">Detail <img src="../../src/icon/icon-detail.png" alt="" class="icon-detail"></a>
								</div>
							</div>
						</div>
					<?php
					}
				} else {
					?>
					<div class="peringatan text-center">
						<p>Tidak Ada Data Aset Wesel</p>
					</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>