<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/editdetaildataaset-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Detail Data Aset Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">Data Aset Wesel-Edit Detail Aset Wesel</p>
			<?php
			} else {
			?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>

		<div class="card mb-4">
			<?php
			$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
			$sqlWesel = mysqli_query($db, $queryWesel);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
				$dataWesel = mysqli_fetch_array($sqlWesel);
			?>
				<form action="confEditDetailDataAset.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" enctype="multipart/form-data" method="POST" >
					<div class="card-header">
						<h4 class="wesel">WESEL <?php echo $dataWesel['noWesel'] ?></h4>
					</div>
					<div class="card-body">
						<div class="row g-3">
							<div class="col-md-6">
								<label for="inputEmail4" class="form-label">Posisi Ujung Wesel</label>
								<input type="text" class="form-control" name="id" value="<?php echo $dataWesel['id'] ?>" style="display: none;">
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Posisi Ujung Wesel" name="posisiUjung" value="<?php echo $dataWesel['posisiUjung'] ?>" required>
							</div>
							<div class="col-md-6">
								<label for="inputEmail4" class="form-label">Posisi Pangkal Wesel</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Posisi Pangkal Wesel" name="posisiPangkal" value="<?php echo $dataWesel['posisiPangkal'] ?>" required>
							</div>
							<div class="col-md-3">
								<label for="inputEmail4" class="form-label">Sudut Wesel</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Sudut Wesel" name="sudutWesel" value="<?php echo $dataWesel['sudutWesel'] ?>" required>
							</div>
							<div class="col-md-6">
								<label for="inputEmail4" class="form-label">Merk Wesel</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Merk Wesel" name="merk" value="<?php echo $dataWesel['merk'] ?>" required>
							</div>
							<div class="col-md-3">
								<label for="inputEmail4" class="form-label">Arah Wesel</label>
								<select class="form-select" aria-label="Default select example" name="arah" required>
									<option selected value="<?php echo $dataWesel['arah'] ?>"><?php echo $dataWesel['arah'] ?></option>
									<option value="Kanan">Kanan</option>
									<option value="Kiri">Kiri</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="inputEmail4" class="form-label">Tipe Rel</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Tipe Rel" name="tipeRel" value="<?php echo $dataWesel['tipeRel'] ?>" required>
							</div>
							<div class="col-md-3">
								<label for="inputEmail4" class="form-label">Lidah Wesel</label>
								<select class="form-select" aria-label="Default select example" name="lidah" required>
									<option selected value="<?php echo $dataWesel['lidah'] ?>"><?php echo $dataWesel['lidah'] ?></option>
									<option value="Pegas">Pegas</option>
									<option value="Putar">Putar</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="inputEmail4" class="form-label">Terlayan</label>
								<select class="form-select" aria-label="Default select example" name="terlayan" required>
									<option selected value="<?php echo $dataWesel['terlayan'] ?>"><?php echo $dataWesel['terlayan'] ?></option>
									<option value="Pusat">Pusat</option>
									<option value="Setempat">Setempat</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="inputEmail4" class="form-label">Tahun Produksi</label>
								<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Tahun Produksi" name="tahun" value="<?php echo $dataWesel['tahun'] ?>" required>
							</div>
							<div class="col-md-12">
								<label for="inputEmail4" class="form-label">Foto Wesel ukuran max 5MB (Kosongkan Jika Tidak Ingin Mengganti Foto)</label>
								<input class="form-control" type="file" id="fotoWesel" name="fotoWesel">
							</div>
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary" value="edit" name="edit">Simpan</button>
							</div>
						</div>
					</div>
				</form>
			<?php
			}
			?>
		</div>
		<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>