<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
    header("location:../../login");
    exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (isset($_POST['save'])){
    $id = $_POST['id'];
    $op_hasilJarum = $_POST['op_hasilJarum'];
    $op_hasilVangrel = $_POST['op_hasilVangrel'];
    $op_hasilLidah = $_POST['op_hasilLidah'];
    $op_hasilRelLantak = $_POST['op_hasilRelLantak'];
    $op_hasilBantalan = $_POST['op_hasilBantalan'];
    $op_hasilPenambat = $_POST['op_hasilPenambat'];

    $op_hasilJarum_fotoNama = $_FILES['op_hasilJarum_foto']['name'];
    $op_hasilJarum_fotoTmp = $_FILES['op_hasilJarum_foto']['tmp_name'];
    $op_hasilJarum_fotoNamaBaru = $id . $op_hasilJarum_fotoNama;
    $op_hasilJarum_fotoSize = $_FILES['op_hasilJarum_foto']['size'];
    $op_hasilJarum_fotoTipe = $_FILES['op_hasilJarum_foto']['type'];
    
    $op_hasilVangrel_fotoNama = $_FILES['op_hasilVangrel_foto']['name'];
    $op_hasilVangrel_fotoTmp = $_FILES['op_hasilVangrel_foto']['tmp_name'];
    $op_hasilVangrel_fotoNamaBaru = $id . $op_hasilVangrel_fotoNama;
    $op_hasilVangrel_fotoSize = $_FILES['op_hasilVangrel_foto']['size'];
    $op_hasilVangrel_fotoTipe = $_FILES['op_hasilVangrel_foto']['type'];

    $op_hasilLidah_fotoNama = $_FILES['op_hasilLidah_foto']['name'];
    $op_hasilLidah_fotoTmp = $_FILES['op_hasilLidah_foto']['tmp_name'];
    $op_hasilLidah_fotoNamaBaru = $id . $op_hasilLidah_fotoNama;
    $op_hasilLidah_fotoSize = $_FILES['op_hasilLidah_foto']['size'];
    $op_hasilLidah_fotoTipe = $_FILES['op_hasilLidah_foto']['type'];

    $op_hasilRelLantak_fotoNama = $_FILES['op_hasilRelLantak_foto']['name'];
    $op_hasilRelLantak_fotoTmp = $_FILES['op_hasilRelLantak_foto']['tmp_name'];
    $op_hasilRelLantak_fotoNamaBaru = $id . $op_hasilRelLantak_fotoNama;
    $op_hasilRelLantak_fotoSize = $_FILES['op_hasilRelLantak_foto']['size'];
    $op_hasilRelLantak_fotoTipe = $_FILES['op_hasilRelLantak_foto']['type'];

    $op_hasilBantalan_fotoNama = $_FILES['op_hasilBantalan_foto']['name'];
    $op_hasilBantalan_fotoTmp = $_FILES['op_hasilBantalan_foto']['tmp_name'];
    $op_hasilBantalan_fotoNamaBaru = $id . $op_hasilBantalan_fotoNama;
    $op_hasilBantalan_fotoSize = $_FILES['op_hasilBantalan_foto']['size'];
    $op_hasilBantalan_fotoTipe = $_FILES['op_hasilBantalan_foto']['type'];

    $op_hasilPenambat_fotoNama = $_FILES['op_hasilPenambat_foto']['name'];
    $op_hasilPenambat_fotoTmp = $_FILES['op_hasilPenambat_foto']['tmp_name'];
    $op_hasilPenambat_fotoNamaBaru = $id . $op_hasilPenambat_fotoNama;
    $op_hasilPenambat_fotoSize = $_FILES['op_hasilPenambat_foto']['size'];
    $op_hasilPenambat_fotoTipe = $_FILES['op_hasilPenambat_foto']['type'];

    if (($op_hasilJarum_fotoTipe == "image/jpeg" || $op_hasilJarum_fotoTipe == "image/png") && 
    ($op_hasilVangrel_fotoTipe == "image/jpeg" || $op_hasilVangrel_fotoTipe == "image/png") &&
    ($op_hasilLidah_fotoTipe == "image/jpeg" || $op_hasilLidah_fotoTipe == "image/png") &&
    ($op_hasilRelLantak_fotoTipe == "image/jpeg" || $op_hasilRelLantak_fotoTipe == "image/png") &&
    ($op_hasilBantalan_fotoTipe == "image/jpeg" || $op_hasilBantalan_fotoTipe == "image/png") && 
    ($op_hasilPenambat_fotoTipe == "image/jpeg" || $op_hasilPenambat_fotoTipe == "image/png")){
        if (($op_hasilJarum_fotoSize <= 5000000) && 
        ($op_hasilVangrel_fotoSize <= 5000000) &&
        ($op_hasilLidah_fotoSize <= 5000000) &&
        ($op_hasilRelLantak_fotoSize <= 5000000) &&
        ($op_hasilBantalan_fotoSize <= 5000000) &&
        ($op_hasilPenambat_fotoSize <= 5000000)){
            if ((move_uploaded_file($op_hasilJarum_fotoTmp, "../../src/gambarPerawatan/" . $op_hasilJarum_fotoNamaBaru)) &&
            (move_uploaded_file($op_hasilVangrel_fotoTmp, "../../src/gambarPerawatan/" . $op_hasilVangrel_fotoNamaBaru)) &&
            (move_uploaded_file($op_hasilLidah_fotoTmp, "../../src/gambarPerawatan/" . $op_hasilLidah_fotoNamaBaru)) && 
            (move_uploaded_file($op_hasilRelLantak_fotoTmp, "../../src/gambarPerawatan/" . $op_hasilRelLantak_fotoNamaBaru)) &&
            (move_uploaded_file($op_hasilBantalan_fotoTmp, "../../src/gambarPerawatan/" . $op_hasilBantalan_fotoNamaBaru)) &&
            (move_uploaded_file($op_hasilPenambat_fotoTmp, "../../src/gambarPerawatan/" . $op_hasilPenambat_fotoNamaBaru))){
                $query = "UPDATE tbl_pemeriksaan_op SET op_hasilJarum = '$op_hasilJarum',
                op_hasilJarum_foto = '$op_hasilJarum_fotoNamaBaru',
                op_hasilVangrel = '$op_hasilVangrel',
                op_hasilVangrel_foto = '$op_hasilVangrel_fotoNamaBaru',
                op_hasilLidah = '$op_hasilLidah',
                op_hasilLidah_foto = '$op_hasilLidah_fotoNamaBaru',
                op_hasilRelLantak = '$op_hasilRelLantak',
                op_hasilRelLantak_foto = '$op_hasilRelLantak_fotoNamaBaru',
                op_hasilBantalan = '$op_hasilBantalan',
                op_hasilBantalan_foto = '$op_hasilBantalan_fotoNamaBaru',
                op_hasilPenambat = '$op_hasilPenambat',
                op_hasilPenambat_foto  = '$op_hasilPenambat_fotoNamaBaru' WHERE id='$id'";
                $sql = mysqli_query($db,$query);
                $querySelesai = "UPDATE tbl_pemeriksaan set `status`='selesai' WHERE id='$id'";
                $sqlSelesai = mysqli_query($db, $querySelesai);
                if($sql && $sqlSelesai){
                    echo "
                        <script>
                            alert('DATA PERAWATAN BERHASIL DI SUBMIT');
                            document.location.href = '../preview-perawatan/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "&id=".$id."';
                        </script>
                        ";
                }else{
                    echo "
                        <script>
                            alert('DATA PERAWATAN GAGAL DI SUBMIT');
                            document.location.href = '../perawatan-opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "&id=".$id."';
                        </script>
                        ";
                }
            }else{
                echo "
                    <script>
                        alert('GAGAL UPLOAD DATA GAMBAR KE SERVER');
                        document.location.href = '../perawatan-opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "&id=".$id."';
                    </script>
                    ";
            }
        }else{
            echo "
                <script>
                    alert('GAMBAR TIDAK BOLEH LEBIH DARI 5 MB');
                    document.location.href = '../perawatan-opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "&id=".$id."';
                </script>
                ";
        }
    }else{
        echo "
            <script>
                alert('HANYA BOLEH MENG-UPLOAD GAMBAR');
                document.location.href = '../perawatan-opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "&id=".$id."';
            </script>
            ";
    }
}