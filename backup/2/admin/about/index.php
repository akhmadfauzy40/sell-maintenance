<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/about-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>About - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset/"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="./" class="active"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="row row-cols-1 row-cols-md-2 g-4 mb-4">
			<div class="col">
				<div class="card">
					<h5 class="card-header">Tentang Website</h5>
					<div class="card-body">
						<p class="card-text">
							Aplikasi SELL Maintenance terdiri 2 kata yang memiliki makna, kata 'SELL' berarti singkatan dari weSEL dan Lengkung serta kata 'Maintenance' dalam bahasa inggris artinya perawatan. Berdasarkan istilah dan makna dari SELL Maintenance berarti aplikasi ini membantu unit  Jalan Rel dan Jembatan (JJ) dalam tugas pokoknya mengenai merumuskan, menyusun dan melaksanakan program pemeliharaan jalan rel, sepur simpang dan jembatan, serta mengevaluasi kinerja pemeliharaan jalan rel, sepur simpang dan jembatan dan pengoprasian fasilitas sarana pemeliharaan jalan rel (MPJR) dan jembatan di seluruh wilayah Daerah Operasi 5 Purwokerto.  Pada aplikasi SELL Maintenance dapat melaksanakan prosedur pemeriksaan wesel, menginput data terkait pemeriksaan wesel sesuai dengan D.145, menyimpan dan mengarsipkan D.145, serta yang terpenting yaitu membantu dalam mengevaluasi hasil pemeriksaan wesel. Aplikasi ini dibuat pada tahun 2022 sebuah karya mahasiswa Institut Teknologi Telkom Purwokerto yang menjadi anak magang pada unit Sistem Informasi dan Unit Jalan Rel dan Jembatan Daerah Operasi 5 Purwokerto.
						</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card">
					<h5 class="card-header">Versi Website</h5>
					<div class="card-body">
						<p class="card-text">
							Version: 1.0.0 (Website)<br>
							Date: 2023-01-22<br>
							Requirement: Gunakan web browser Chrome, Edge, atau Firefox<br>
							Bootstrap: V5.2 <br>
							OS: Windows_NT x64 10.0.22621 <br>
						</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card">
					<h5 class="card-header">Tim Develop Website</h5>
					<div class="card-body">
						<p class="card-text">
							Renita Fauziah Samodra | Linda Ayu Kusuma Ningrum | Akhmad Fauzi | Edoardus Dwijo Wijayanto | Ardha Mevia Audri<br><br>
							Sistem Informasi | Institut Teknologi Telkom Purwokerto<br><br>
							Unit Sistem Informasi | Unit Jalan Rel dan Jembatan<br><br>
							DAOP 5 Purwokerto | PT Kereta Api Indonesia (Persero)
						</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card">
					<h5 class="card-header">Kontak</h5>
					<div class="card-body">
						<p class="card-text">
							<div class="developer">
								<p>Developer</p>
								<div class="tlp">
									<img src="../../src/icon/icon-whatsapp.png" alt="" class="telpon">
									<a href="https://api.whatsapp.com/send?phone=6281225069824">Renita Fauziah Samodra</a>
								</div>
								<div class="mail">
									<img src="../../src/icon/icon-mail.png" alt="" class="email">
									<a href="mailto:renitafauziahsamodra@gmail.com">renitafauziahsamodra@gmail.com</a>
								</div>
							</div>
							<div class="divider"></div>
							<div class="kai">
								<p>Sistem Informasi DAOP 5 Purwokerto</p>
								<div class="tlp">
									<img src="../../src/icon/icon-whatsapp.png" alt="" class="telpon">
									<a href="https://api.whatsapp.com/send?phone=628157915943">Pitra Argehermanu</a>
								</div>
								<div class="mail">
									<img src="../../src/icon/icon-mail.png" alt="" class="email">
									<a href="mailto:pitra.argehermanu@kai.id">pitra.argehermanu@kai.id</a>
								</div>
							</div>
						</p>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>