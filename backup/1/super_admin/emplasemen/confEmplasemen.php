<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
    header("location:../login");
    exit;
}

if (isset($_POST['save'])) {
    $kodeResort = $_POST['kodeResort'];
    $namaEmplasemen = $_POST['namaEmplasemen'];
    $alamatEmplasemen = $_POST['alamatEmplasemen'];

    $fotoEmplasemenNama = $_FILES['fotoEmplasemen']['name'];
    $fotoEmplasemenTmp = $_FILES['fotoEmplasemen']['tmp_name'];
    $fotoEmplasemenNamaBaru = $namaEmplasemen . $fotoEmplasemenNama;
    $fotoEmplasemenSize = $_FILES['fotoEmplasemen']['size'];
    $fotoEmplasemenTipe = $_FILES['fotoEmplasemen']['type'];

    if ($fotoEmplasemenTipe == "image/jpeg" || $fotoEmplasemenTipe == "image/png") {
        if ($fotoEmplasemenSize <= 5000000) {
            if (move_uploaded_file($fotoEmplasemenTmp, "../../src/img/" . $fotoEmplasemenNamaBaru)) {
                $query = "INSERT INTO tbl_emplasemen (kodeResort, namaEmplasemen, alamatEmplasemen, gambarEmplasemen) VALUE ('$kodeResort', '$namaEmplasemen', '$alamatEmplasemen', '$fotoEmplasemenNamaBaru')";
                $sql = mysqli_query($db, $query);

                if ($sql) {
                    echo "
                        <script>
                            alert('DATA BARU BERHASIL DI INPUT');
                            document.location.href = '../emplasemen';
                        </script>
                    ";
                } else {
                    echo "
                        <script>
                            alert('DATA GAGAL DI INPUT');
                            document.location.href = '../emplasemen';
                        </script>
                    ";
                }
            }
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI INPUT, GAMBAR TIDAK BOLEH LEBIH DARI 5MB');
                        document.location.href = '../emplasemen';
                    </script>
                ";
        }
    } else {
        echo "
                <script>
                    alert('DATA GAGAL DI INPUT, TYPE FILE YANG ANDA UPLOAD BUKAN GAMBAR');
                    document.location.href = '../emplasemen';
                </script>
            ";
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $kodeResort = $_POST['kodeResort'];
    $namaEmplasemen = $_POST['namaEmplasemen'];
    $alamatEmplasemen = $_POST['alamatEmplasemen'];

    if ($_FILES['fotoEmplasemen']['tmp_name'] != '') {
        $fotoEmplasemenNama = $_FILES['fotoEmplasemen']['name'];
        $fotoEmplasemenTmp = $_FILES['fotoEmplasemen']['tmp_name'];
        $fotoEmplasemenNamaBaru = $namaEmplasemen . $fotoEmplasemenNama;
        $fotoEmplasemenSize = $_FILES['fotoEmplasemen']['size'];
        $fotoEmplasemenTipe = $_FILES['fotoEmplasemen']['type'];

        if ($fotoEmplasemenTipe == "image/jpeg" || $fotoEmplasemenTipe == "image/png") {
            if ($fotoEmplasemenSize <= 5000000) {
                if (move_uploaded_file($fotoEmplasemenTmp, "../../src/img/" . $fotoEmplasemenNamaBaru)) {
                    $query = "UPDATE tbl_emplasemen SET kodeResort='$kodeResort', namaEmplasemen='$namaEmplasemen', alamatEmplasemen='$alamatEmplasemen', gambarEmplasemen='$fotoEmplasemenNamaBaru' WHERE id=$id";
                    $sql = mysqli_query($db, $query);

                    if ($sql) {
                        echo "
                            <script>
                                alert('DATA BERHASIL DI EDIT');
                                document.location.href = '../emplasemen';
                            </script>
                        ";
                    } else {
                        echo "
                            <script>
                                alert('DATA GAGAL DI EDIT');
                                document.location.href = '../emplasemen';
                            </script>
                        ";
                    }
                }
            } else {
                echo "
                        <script>
                            alert('DATA GAGAL DI INPUT, GAMBAR TIDAK BOLEH LEBIH DARI 5MB');
                            document.location.href = '../emplasemen';
                        </script>
                    ";
            }
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI INPUT, TYPE FILE YANG ANDA UPLOAD BUKAN GAMBAR');
                        document.location.href = '../emplasemen';
                    </script>
                ";
        }
    } else {
        $query = "UPDATE tbl_emplasemen SET kodeResort='$kodeResort', namaEmplasemen='$namaEmplasemen', alamatEmplasemen='$alamatEmplasemen' WHERE id=$id";
        $sql = mysqli_query($db, $query);

        if ($sql) {
            echo "
                    <script>
                        alert('DATA BERHASIL DI EDIT');
                        document.location.href = '../emplasemen';
                    </script>
                ";
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI EDIT');
                        document.location.href = '../emplasemen';
                    </script>
                ";
        }
    }
}

if (isset($_POST['hapus'])) {
    $id = $_POST['id'];

    $pickData = "SELECT * FROM tbl_emplasemen WHERE id='$id'";
    $sqlPickData = mysqli_query($db, $pickData);

    if ($data = mysqli_fetch_array($sqlPickData)) {
        $namaEmplasemen = $data['namaEmplasemen'];
        $queryChecker = "SELECT * FROM tbl_data_aset WHERE emplasemen='$namaEmplasemen'";
        $sqlChecker = mysqli_query($db, $queryChecker);

        if (mysqli_num_rows($sqlChecker) != 0) {
            echo "
                <script>
                    alert('SEBELUM MENGHAPUS EMPLASEMEN INI, HARAP HAPUS DULU DATA WESEL YANG TERMASUK DALAM LINGKUP EMPLASEMEN INI');
                    document.location.href = '../emplasemen';
                </script>
            ";
        } else {
            $query = "DELETE FROM tbl_emplasemen WHERE id='$id'";
            $sql = mysqli_query($db, $query);

            if ($sql) {
                echo "
            <script>
                alert('DATA BERHASIL DI HAPUS');
                document.location.href = '../emplasemen';
            </script>
        ";
            } else {
                echo "
            <script>
                alert('DATA GAGAL DI HAPUS');
                document.location.href = '../emplasemen';
            </script>
        ";
            }
        }
    }
}
