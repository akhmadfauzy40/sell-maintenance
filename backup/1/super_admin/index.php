<?php
include("../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
	header("location:../login");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../css/home-superadmin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Home - Super Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../src/image/kaiLogo.png" alt="">
		</div>
		<a class="active" href="./"><img src="../src/icon/icon-user.png" alt="" class="icon">User</a>
		<a href="#setting" data-bs-toggle="collapse"><img src="../src/icon/icon-setting.png" alt="" class="icon">Setting</a>
		<div class="collapse sub-menu" id="setting">
			<a href="setting/"><img src="../src/icon/icon-home.png" alt="" class="icon">Setting Home</a>
		</div>
		<a href="#resort" data-bs-toggle="collapse"><img src="../src/icon/icon-resort.png" alt="" class="icon">Resor</a>
		<div class="collapse sub-menu" id="resort">
			<a href="data-resort/"><img src="../src/icon/icon-dataresort.png" alt="" class="icon">Data Resor</a>
			<a href="emplasemen/"><img src="../src/icon/icon-emplasemen.png" alt="" class="icon">Emplasemen</a>
			<a href="data-aset/"><img src="../src/icon/icon-dataaset.png" alt="" class="icon">Data Aset Wesel</a>
		</div>

		<a href="logout.php"><img src="../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="tambah-user">
			<button type="button" class="btn btn-primary btn-sm shadow-sm" data-bs-toggle="modal" data-bs-target="#tambahUser">Tambah User</button>

			<div class="modal fade" id="tambahUser" tabindex="-1" aria-labelledby="tambahUserLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tambahUserLabel">Tambah User</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<form action="confAdmin.php" method="post">
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Nama</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Nama Lengkap" name="nama" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">NIPP</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan NIPP" name="nipp" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Kode Resor</label>
									<select class="form-select" aria-label="Default select example" name="kodeResort" required>
										<option selected disabled value="">Pilih Kode Resor</option>
										<?php
										$sqlSelect = "SELECT * FROM tbl_resort";
										$querySelect = mysqli_query($db, $sqlSelect);
										while ($dataSelect = mysqli_fetch_array($querySelect)) {
										?>
											<option value="<? echo $dataSelect['kodeResort'] ?>"><?php echo $dataSelect['kodeResort'] ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Jabatan</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Jabatan" name="jabatan" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">No Telepon</label>
									<input type="tel" class="form-control" id="recipient-name" placeholder="Masukkan No.Telepon" name="no_hp" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Email</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Email" name="email" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Role</label>
									<select class="form-select" aria-label="Default select example" name="role" required>
										<option selected disabled value="">Pilih Role</option>
										<option value="super admin">Super Admin</option>
										<option value="admin">Admin</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Password</label>
									<input type="password" class="form-control" id="recipient-name" placeholder="Masukkan Password" name="password" required>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
									<button class="btn btn-primary" type="submit" value="save" name="save">Tambah</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tabel table-responsive">
			<table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th scope="col" class="text-center">No</th>
						<th scope="col">NIPP</th>
						<th scope="col">Nama</th>
						<th scope="col">Kode Resort</th>
						<th scope="col">Jabatan</th>
						<th scope="col">No. Telepon</th>
						<th scope="col">Email</th>
						<th scope="col">Role</th>
						<th scope="col" class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_user";
					$query = mysqli_query($db, $sql);
					$no = 1;

					while ($data = mysqli_fetch_array($query)) {
						echo "<tr>";

						echo "<td class='text-center'>" . $no . "</td>";
						echo "<td>" . $data['nipp'] . "</td>";
						echo "<td>" . $data['nama'] . "</td>";
						echo "<td>" . $data['kodeResort'] . "</td>";
						echo "<td>" . $data['jabatan'] . "</td>";
						echo "<td>" . $data['no_hp'] . "</td>";
						echo "<td>" . $data['email'] . "</td>";
						echo "<td>" . $data['role'] . "</td>";

					?>
						<td class='align-middle'>
							<div class='d-flex justify-content-center'>
								<button type='button' class='btn btn-primary btn-sm shadow-sm' data-bs-toggle='modal' data-bs-target='#editModal<?php echo $no ?>'>Edit</button>&nbsp
								<div class='modal fade' id='editModal<?php echo $no ?>' tabindex='-1' aria-labelledby='editModalLabel' aria-hidden='true'>
									<div class='modal-dialog modal-lg'>
										<div class='modal-content'>
											<div class='modal-header'>
												<h5 class='modal-title' id='editModalLabel'>Edit</h5>
												<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
											</div>
											<div class='modal-body'>
												<form action='confAdmin.php' method='post'>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Nama</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Nama Lengkap' name='nama' value='<?php echo $data['nama'] ?>' required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>NIPP</label>
														<input type='text' class='form-control' id='recipient-name' name='nipp' value='<?php echo $data['nipp'] ?>' readonly>
													</div>
													<div class="mb-3">
														<label for="recipient-name" class="col-form-label">Kode Resor</label>
														<select class="form-select" aria-label="Default select example" name="kodeResort" required>
															<option selected value="<?php echo $data['kodeResort'] ?>"><?php echo $data['kodeResort'] ?></option>
															<?php
															$sqlSelect = "SELECT * FROM tbl_resort";
															$querySelect = mysqli_query($db, $sqlSelect);
															while ($dataSelect = mysqli_fetch_array($querySelect)) {
															?>
																<option value="<?php echo $dataSelect['kodeResort'] ?>"><?php echo $dataSelect['kodeResort'] ?></option>
															<?php
															}
															?>
														</select>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Jabatan</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Jabatan' name='jabatan' value='<?php echo $data['jabatan'] ?>' required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>No Telepon</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan No. Telepon' name='no_hp' value='<?php echo $data['no_hp'] ?>' required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Email</label>
														<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan NIPP' name='email' value='<?php echo $data['email'] ?>' required>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Role</label>
														<input type='text' class='form-control' id='recipient-name' name='role' value='<?php echo $data['role'] ?>' readonly>
													</div>
													<div class='mb-3'>
														<label for='recipient-name' class='col-form-label'>Password</label>
														<input type='password' class='form-control' id='recipient-name' placeholder='Masukkan Password / Kosongkan Jika Tidak Ingin Mengganti Password' name='password'>
														<input type='password' class='form-control' id='recipient-name' value='<?php echo $data['password'] ?>' name='passBefore' style='display: none;'>
													</div>
													<div class='modal-footer'>
														<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
														<button class='btn btn-primary' type='submit' value='edit' name='edit'>Simpan</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								<button type='button' class='btn btn-danger btn-sm shadow-sm' data-bs-toggle='modal' data-bs-target='#hapusModal<?php echo $no ?>'>Hapus</button>
								<div class='modal fade' id='hapusModal<?php echo $no ?>' tabindex='-1' aria-labelledby='hapusModalLabel' aria-hidden='true'>
									<div class='modal-dialog'>
										<div class='modal-content'>
											<div class='modal-header'>
												<h5 class='modal-title' id='hapusModalLabel'>Hapus</h5>
												<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
											</div>
											<form action='confAdmin.php' method='post'>
												<div class='modal-body'>
													Apakah anda yakin ingin menghapus data ini?
													<input type='text' class='form-control' id='recipient-name' name='nipp' value='<?php echo $data['nipp'] ?>' readonly style='display:none;'>
												</div>
												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
													<button type='submit' class='btn btn-danger' value='hapus' name='hapus'>Hapus</button>
											</form>
										</div>
									</div>
								</div>
							</div>
		</div>
		</td>
	<?php
						echo "</tr>";
						$no++;
					}
	?>
	</tbody>
	</table>
	</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>