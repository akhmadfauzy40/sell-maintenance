<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
	header("location:../../login");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/dataresort-superadmin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Data Resor - Super Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-user.png" alt="" class="icon">User</a>
		<a href="#setting" data-bs-toggle="collapse"><img src="../../src/icon/icon-setting.png" alt="" class="icon">Setting</a>
		<div class="collapse sub-menu" id="setting">
			<a href="../setting/"><img src="../../src/icon/icon-home.png" alt="" class="icon">Setting Home</a>
		</div>
		<a href="#resort" data-bs-toggle="collapse" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Resor</a>
		<div class="collapse sub-menu" id="resort">
			<a href="./" class="active"><img src="../../src/icon/icon-dataresort.png" alt="" class="icon">Data Resor</a>
			<a href="../emplasemen/"><img src="../../src/icon/icon-emplasemen.png" alt="" class="icon">Emplasemen</a>
			<a href="../data-aset"><img src="../../src/icon/icon-dataaset.png" alt="" class="icon">Data Aset Wesel</a>
		</div>

		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="tambah-user">
			<button type="button" class="btn btn-primary btn-sm shadow-sm" data-bs-toggle="modal" data-bs-target="#tambahResort">Tambah Resor</button>

			<div class="modal fade" id="tambahResort" tabindex="-1" aria-labelledby="tambahResortLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tambahResortLabel">Tambah Resor</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<form action="confResort.php" method="post">
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Nama Resor</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Nama Resor" name="namaResort" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Kode Resor</label>
									<input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Kode Resor" name="kodeResort" required>
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Alamat</label>
									<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat" placeholder="Masukkan Alamat" required></textarea>
									<!-- <input type="text" class="form-control" id="recipient-name" placeholder="Masukkan Alamat" name="alamat" required> -->
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
									<button class="btn btn-primary" type="submit" value="save" name="save">Tambah</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tabel table-responsive">
			<table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
				<thead class="table-warning">
					<tr>
						<th scope="col" width="5%">No</th>
						<th scope="col" width="15%">Kode</th>
						<th scope="col" width="35%">Nama Resor</th>
						<th scope="col">Alamat</th>
						<th scope="col" class="text-center" width="20%">Aksi</th>
						</tr>
				</thead>
				<tbody>
				<?php
				$sql = "SELECT * FROM tbl_resort";
				$query = mysqli_query($db, $sql);
				$no = 1;

				while ($data = mysqli_fetch_array($query)) {
					echo "<tr>";

					echo "<td class='text-center'>" . $no . "</td>";
					echo "<td>" . $data['kodeResort'] . "</td>";
					echo "<td>" . $data['namaResort'] . "</td>";
					echo "<td>" . $data['alamat'] . "</td>";

					echo "<td class='align-middle'>";
					echo "<div class='d-flex justify-content-center'>";
					echo "<button type='button' class='btn btn-primary btn-sm shadow-sm tombol' data-bs-toggle='modal' data-bs-target='#editModal" . $no . "'>Edit</button>&nbsp";
					echo "<div class='modal fade' id='editModal" . $no . "' tabindex='-1' aria-labelledby='editModalLabel' aria-hidden='true'>
						  <div class='modal-dialog modal-lg'>
						    <div class='modal-content'>
						      <div class='modal-header'>
						        <h5 class='modal-title' id='editModalLabel'>Edit</h5>
						        <button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						      </div>
						      <div class='modal-body'>
						        <form action='confResort.php' method='post'>
								<input type='text' class='form-control' id='recipient-name' name='id' value='" . $data['id'] . "' style='display:none;'>
									<div class='mb-3'>
										<label for='recipient-name' class='col-form-label'>Nama Resor</label>
										<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Nama Resor' name='namaResort' value='" . $data['namaResort'] . "' required>
									</div>
									<div class='mb-3'>
										<label for='recipient-name' class='col-form-label'>Kode Resort</label>
										<input type='text' class='form-control' id='recipient-name' placeholder='Masukkan Kode Resort' name='kodeResort' value='" . $data['kodeResort'] . "' required>
									</div>
									<div class='mb-3'>
										<label for='recipient-name' class='col-form-label'>Alamat</label>
										<textarea class='form-control' id='exampleFormControlTextarea1' rows='3' name='alamat' placeholder='Masukkan Alamat' required>" . $data['alamat'] . "</textarea>
									</div>
									<div class='modal-footer'>
						        		<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
						        		<button class='btn btn-primary' type='submit' value='edit' name='edit'>Simpan</button>
						      		</div>
								</form>
						      </div>
						    </div>
						  </div>
						</div>";
					echo "<button type='button' class='btn btn-danger btn-sm shadow-sm tombol' data-bs-toggle='modal' data-bs-target='#hapusModal" . $no . "'>Hapus</button>";
					echo "<div class='modal fade' id='hapusModal" . $no . "' tabindex='-1' aria-labelledby='hapusModalLabel' aria-hidden='true'>
						  <div class='modal-dialog'>
						    <div class='modal-content'>
						      <div class='modal-header'>
						        <h5 class='modal-title' id='hapusModalLabel'>Hapus</h5>
						        <button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						      </div>
							  <form action='confResort.php' method='post'>
						      <div class='modal-body'>
						        Apakah anda yakin ingin menghapus data ini?
								<input type='text' class='form-control' id='recipient-name' name='id' value='" . $data['id'] . "' style='display:none;'>
						      </div>
						      <div class='modal-footer'>
						        <button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
						        <button class='btn btn-danger' type='submit' class='btn btn-danger' value='hapus' name='hapus'>Hapus</button>
						      </div>
							  </form>
						    </div>
						  </div>
						</div>";
					echo "</div>";
					echo "</td>";

					echo "</tr>";
					$no++;
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function(){
		$('#tbl_user').DataTable();
	});
</script>
</html>