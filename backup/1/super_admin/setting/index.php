<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
	header("location:../../login");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/setting-superadmin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Setting - Super Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../../super_admin/"><img src="../../src/icon/icon-user.png" alt="" class="icon">User</a>
		<a href="#setting" data-bs-toggle="collapse" class="active"><img src="../../src/icon/icon-setting.png" alt="" class="icon">Setting</a>
		<div class="collapse sub-menu" id="setting">
			<a href="./" class="active"><img src="../../src/icon/icon-home.png" alt="" class="icon">Setting Home</a>
		</div>
		<a href="#resort" data-bs-toggle="collapse"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Resor</a>
		<div class="collapse sub-menu" id="resort">
			<a href="../data-resort/"><img src="../../src/icon/icon-dataresort.png" alt="" class="icon">Data Resor</a>
			<a href="../emplasemen/"><img src="../../src/icon/icon-emplasemen.png" alt="" class="icon">Emplasemen</a>
			<a href="../data-aset/"><img src="../../src/icon/icon-dataaset.png" alt="" class="icon">Data Aset Wesel</a>
		</div>

		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="header text-center">
			<p>Jalan Rel dan Jembatan<br>DAOP 5 Purwokerto</p>
		</div>
		<div class="struktur text-center">
			<?php
			$query = "SELECT * FROM home WHERE id=1";
			$sql = mysqli_query($db, $query);

			if ($data = mysqli_fetch_array($sql)) {
			?> <img src="../../src/img/<?php echo $data['strukturOrg'] ?>" alt=""> <?php
																						}
																							?>
		</div>
		<div class="edit-gambar d-flex justify-content-end">
			<button type="button" class="btn btn-primary btn-sm shadow-sm" data-bs-toggle="modal" data-bs-target="#editGambar">Edit Gambar</button>

			<div class="modal fade" id="editGambar" tabindex="-1" aria-labelledby="tambahUserLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tambahUserLabel">Edit Gambar</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<form action="confSetting.php" enctype="multipart/form-data" method="post">
								<div class="mb-3">
								<input type='text' class='form-control' id='recipient-name' name='id' value="<?php echo $data['id'] ?>" readonly style='display:none;'>
									<label for="formFile" class="form-label">Gambar Struktur Organisasi ukuran max 5mb</label>
									<input class="form-control" type="file" id="strukturOrg" name="strukturOrg" required>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
									<button class="btn btn-primary" type="submit" value="saveGambar" name="saveGambar">Save</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="deskripsi">
			<div class="row">
				<div class="col-sm-6">
					<div class="card">
						<h5 class="card-header text-center">Deskripsi Unit JJ</h5>
						<div class="card-body">
							<p class="card-text text-center">
								<?php echo $data['deskripsi'] ?>
							</p>
							<button type="button" class="btn btn-primary btn-sm shadow-sm" data-bs-toggle="modal" data-bs-target="#editDeskripsi">Edit</button>

							<div class="modal fade" id="editDeskripsi" tabindex="-1" aria-labelledby="tambahUserLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="tambahUserLabel">Edit Deskripsi</h5>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<div class="modal-body">
											<form action="confSetting.php" method="post">
												<div class="mb-3">
												<input type='text' class='form-control' id='recipient-name' name='id' value="<?php echo $data['id'] ?>" readonly style='display:none;'>
													<label for="recipient-name" class="col-form-label">Deskripsi</label>
													<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="deskripsi" placeholder="Masukkan Deskripsi" required><?php echo $data['deskripsi'] ?></textarea>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
													<button class="btn btn-primary" type="submit" value="editDesk" name="editDesk">Save</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 scroll">
					<div class="card">
						<h5 class="card-header text-center">Daftar Resor Unit JJ DAOP 5 Purwokerto</h5>
						<div class="card-body">
						<?php
							$sql = "SELECT * FROM tbl_resort";
							$query = mysqli_query($db, $sql);
							$no = 1;

							while ($data = mysqli_fetch_array($query)){
								?><center><p class="card-text"><?php echo $no.". ".$data['namaResort']; ?></p></center><?php
								$no++;
							}

							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>