<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "superAdmin") {
    header("location:../login");
    exit;
}

if (isset($_POST['saveGambar'])) {
    $id = $_POST['id'];
    $strukturOrgNama = $_FILES['strukturOrg']['name'];
    $strukturOrgTmp = $_FILES['strukturOrg']['tmp_name'];
    $strukturOrgNamaBaru = "strukturOrg" . $strukturOrgNama;
    $strukturOrgSize = $_FILES['strukturOrg']['size'];
    $strukturOrgTipe = $_FILES['strukturOrg']['type'];

    if ($strukturOrgTipe == "image/jpeg" || $strukturOrgtipe == "image/png") {
        if ($strukturOrgSize <= 5000000) {
            if (move_uploaded_file($strukturOrgTmp, "../../src/img/" . $strukturOrgNamaBaru)) {
                $query = "UPDATE home SET strukturOrg='$strukturOrgNamaBaru' WHERE id=$id";
                $sql = mysqli_query($db, $query);

                if ($sql) {
                    echo "
                        <script>
                            alert('GAMBAR BERHASIL DI EDIT');
                            document.location.href = '../setting';
                        </script>
                    ";
                } else {
                    echo "
                        <script>
                            alert('DATA GAGAL DI INPUT');
                            document.location.href = '../setting';
                        </script>
                    ";
                }
            }
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI INPUT, GAMBAR TIDAK BOLEH LEBIH DARI 5MB');
                        document.location.href = '../setting';
                    </script>
                ";
        }
    } else {
        echo "
                <script>
                    alert('DATA GAGAL DI INPUT, TYPE FILE YANG ANDA UPLOAD BUKAN GAMBAR');
                    document.location.href = '../setting';
                </script>
            ";
    }
}

if (isset($_POST['editDesk'])) {
    $id = $_POST['id'];
    $deskripsi = $_POST['deskripsi'];

    $query = "UPDATE home SET deskripsi='$deskripsi' WHERE id=$id";
    $sql = mysqli_query($db, $query);

    if ($sql) {
        echo "
                <script>
                    alert('DESKRIPSI BERHASIL DI EDIT');
                    document.location.href = '../setting';
                </script>
            ";
    } else {
        echo "
                <script>
                    alert('DESKRIPSI GAGAL DI EDIT');
                    document.location.href = '../setting';
                </script>
            ";
    }
}