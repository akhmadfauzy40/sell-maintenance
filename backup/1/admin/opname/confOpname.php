<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
    header("location:../../login");
    exit;
}

$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (!isset($_SESSION['id' . $noWesel])) {
    header("location:../data-aset/");
    exit;
}

if (isset($_POST['save'])) {
    $id = $_SESSION['id' . $noWesel];
    $op_kondisiJarum = $_POST['op_kondisiJarum'];
    $op_kondisiVangrel = $_POST['op_kondisiVangrel'];
    $op_kondisiLidah = $_POST['op_kondisiLidah'];
    $op_kondisiRelLantak = $_POST['op_kondisiRelLantak'];
    $op_kondisiBantalan = $_POST['op_kondisiBantalan'];
    $op_kondisiPenambat = $_POST['op_kondisiPenambat'];
    $nama_kupt = $_POST['nama_kupt'];
    $nipp_kupt = $_POST['nipp_kupt'];
    $noHp_kupt = $_POST['noHp_kupt'];
    $nama_kat = $_POST['nama_kat'];
    $nipp_kat = $_POST['nipp_kat'];
    $noHp_kat = $_POST['noHp_kat'];

    $op_kondisiJarum_fotoNama = $_FILES['op_kondisiJarum_foto']['name'];
    $op_kondisiJarum_fotoTmp = $_FILES['op_kondisiJarum_foto']['tmp_name'];
    $op_kondisiJarum_fotoNamaBaru = $id . $op_kondisiJarum_fotoNama;
    $op_kondisiJarum_fotoSize = $_FILES['op_kondisiJarum_foto']['size'];
    $op_kondisiJarum_fotoTipe = $_FILES['op_kondisiJarum_foto']['type'];

    $op_kondisiVangrel_fotoNama = $_FILES['op_kondisiVangrel_foto']['name'];
    $op_kondisiVangrel_fotoTmp = $_FILES['op_kondisiVangrel_foto']['tmp_name'];
    $op_kondisiVangrel_fotoNamaBaru = $id . $op_kondisiVangrel_fotoNama;
    $op_kondisiVangrel_fotoSize = $_FILES['op_kondisiVangrel_foto']['size'];
    $op_kondisiVangrel_fotoTipe = $_FILES['op_kondisiVangrel_foto']['type'];

    $op_kondisiLidah_fotoNama = $_FILES['op_kondisiLidah_foto']['name'];
    $op_kondisiLidah_fotoTmp = $_FILES['op_kondisiLidah_foto']['tmp_name'];
    $op_kondisiLidah_fotoNamaBaru = $id . $op_kondisiLidah_fotoNama;
    $op_kondisiLidah_fotoSize = $_FILES['op_kondisiLidah_foto']['size'];
    $op_kondisiLidah_fotoTipe = $_FILES['op_kondisiLidah_foto']['type'];

    $op_kondisiRelLantak_fotoNama = $_FILES['op_kondisiRelLantak_foto']['name'];
    $op_kondisiRelLantak_fotoTmp = $_FILES['op_kondisiRelLantak_foto']['tmp_name'];
    $op_kondisiRelLantak_fotoNamaBaru = $id . $op_kondisiRelLantak_fotoNama;
    $op_kondisiRelLantak_fotoSize = $_FILES['op_kondisiRelLantak_foto']['size'];
    $op_kondisiRelLantak_fotoTipe = $_FILES['op_kondisiRelLantak_foto']['type'];

    $op_kondisiBantalan_fotoNama = $_FILES['op_kondisiBantalan_foto']['name'];
    $op_kondisiBantalan_fotoTmp = $_FILES['op_kondisiBantalan_foto']['tmp_name'];
    $op_kondisiBantalan_fotoNamaBaru = $id . $op_kondisiBantalan_fotoNama;
    $op_kondisiBantalan_fotoSize = $_FILES['op_kondisiBantalan_foto']['size'];
    $op_kondisiBantalan_fotoTipe = $_FILES['op_kondisiBantalan_foto']['type'];

    $op_kondisiPenambat_fotoNama = $_FILES['op_kondisiPenambat_foto']['name'];
    $op_kondisiPenambat_fotoTmp = $_FILES['op_kondisiPenambat_foto']['tmp_name'];
    $op_kondisiPenambat_fotoNamaBaru = $id . $op_kondisiPenambat_fotoNama;
    $op_kondisiPenambat_fotoSize = $_FILES['op_kondisiPenambat_foto']['size'];
    $op_kondisiPenambat_fotoTipe = $_FILES['op_kondisiPenambat_foto']['type'];

    if (($op_kondisiJarum_fotoTipe == "image/jpeg" || $op_kondisiJarum_fotoTipe == "image/png") && ($op_kondisiVangrel_fotoTipe == "image/jpeg" || $op_kondisiVangrel_fotoTipe == "image/png") && ($op_kondisiLidah_fotoTipe == "image/jpeg" || $op_kondisiLidah_fotoTipe == "image/png") && ($op_kondisiRelLantak_fotoTipe == "image/jpeg" || $op_kondisiRelLantak_fotoTipe == "image/png") && ($op_kondisiBantalan_fotoTipe == "image/jpeg" || $op_kondisiBantalan_fotoTipe == "image/png") && ($op_kondisiPenambat_fotoTipe == "image/jpeg" || $op_kondisiPenambat_fotoTipe == "image/png")) {
        if (($op_kondisiJarum_fotoSize <= 5000000) && ($op_kondisiVangrel_fotoSize <= 5000000) && ($op_kondisiLidah_fotoSize <= 5000000) && ($op_kondisiRelLantak_fotoSize <= 5000000) && ($op_kondisiBantalan_fotoSize <= 5000000) && ($op_kondisiPenambat_fotoSize <= 5000000)) {
            if ((move_uploaded_file($op_kondisiJarum_fotoTmp, "../../src/gambarPemeriksaan/" . $op_kondisiJarum_fotoNamaBaru)) &&
                (move_uploaded_file($op_kondisiVangrel_fotoTmp, "../../src/gambarPemeriksaan/" . $op_kondisiVangrel_fotoNamaBaru)) &&
                (move_uploaded_file($op_kondisiLidah_fotoTmp, "../../src/gambarPemeriksaan/" . $op_kondisiLidah_fotoNamaBaru)) &&
                (move_uploaded_file($op_kondisiRelLantak_fotoTmp, "../../src/gambarPemeriksaan/" . $op_kondisiRelLantak_fotoNamaBaru)) &&
                (move_uploaded_file($op_kondisiBantalan_fotoTmp, "../../src/gambarPemeriksaan/" . $op_kondisiBantalan_fotoNamaBaru)) &&
                (move_uploaded_file($op_kondisiPenambat_fotoTmp, "../../src/gambarPemeriksaan/" . $op_kondisiPenambat_fotoNamaBaru))
            ) {
                $queryPemeriksaan = "UPDATE tbl_pemeriksaan SET nama_kupt='$nama_kupt', nipp_kupt='$nipp_kupt', noHp_kupt='$noHp_kupt', nama_kat='$nama_kat', nipp_kat='$nipp_kat', noHp_kat='$noHp_kat', `status`='selesaiPemeriksaan' WHERE id='$id'";
                $sqlPemeriksaan = mysqli_query($db, $queryPemeriksaan);
                $queryOp = "INSERT INTO tbl_pemeriksaan_op (id,
                    op_kondisiJarum,
                    op_kondisiJarum_foto,
                    op_kondisiVangrel,
                    op_kondisiVangrel_foto,
                    op_kondisiLidah,
                    op_kondisiLidah_foto,
                    op_kondisiRelLantak,
                    op_kondisiRelLantak_foto,
                    op_kondisiBantalan,
                    op_kondisiBantalan_foto,
                    op_kondisiPenambat,
                    op_kondisiPenambat_foto) VALUE (
                    '$id',
                    '$op_kondisiJarum',
                    '$op_kondisiJarum_fotoNamaBaru',
                    '$op_kondisiVangrel',
                    '$op_kondisiVangrel_fotoNamaBaru',
                    '$op_kondisiLidah',
                    '$op_kondisiLidah_fotoNamaBaru',
                    '$op_kondisiRelLantak',
                    '$op_kondisiRelLantak_fotoNamaBaru',
                    '$op_kondisiBantalan',
                    '$op_kondisiBantalan_fotoNamaBaru',
                    '$op_kondisiPenambat',
                    '$op_kondisiPenambat_fotoNamaBaru')";
                $sqlOp = mysqli_query($db, $queryOp);

                if ($sqlPemeriksaan && $sqlOp) {
                    unset($_SESSION['id' . $noWesel]);
                    echo "
                        <script>
                            alert('DATA PEMERIKSAAN BERHASIL DI SUBMIT');
                            document.location.href = '../preview-pemeriksaan/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "&id=".$id."';
                        </script>
                        ";
                }
            } else {
                echo "
                    <script>
                        alert('GAMBAR GAGAL DI SIMPAN');
                        document.location.href = '../opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "';
                    </script>
                    ";
            }
        } else {
            echo "gambar lebih besar dari 5 MB";
            echo "
                <script>
                    alert('UKURAN GAMBAR LEBIH BESAR DARI 5MB');
                    document.location.href = '../opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "';
                </script>
                ";
        }
    } else {
        echo "yg di upload bukan gambar";
        echo "
                <script>
                    alert('GAMBAR YANG DI UPLOAD HARUS BERJENIS PNG ATAU JPEG');
                    document.location.href = '../opname/?emplasemen=" . $emplasemen . "&resort=" . $resort . "&noWesel=" . $noWesel . "';
                </script>
            ";
    }
}
