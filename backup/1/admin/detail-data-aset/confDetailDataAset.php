<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];
$id = $_GET['id'];

if(isset($_POST['submit'])){
    $tgl = date('dmY');
    $docNama = $_FILES['doc']['name'];
    $docTmp = $_FILES['doc']['tmp_name'];
    $docNamaBaru = $noWesel."-(". $tgl. "-" . $id .").pdf";
    $docSize = $_FILES['doc']['size'];
    $docTipe = $_FILES['doc']['type'];

    if ($docTipe == "application/pdf"){
        if (move_uploaded_file($docTmp, "../../src/doc/" . $docNamaBaru)){
            $query = "INSERT INTO doc_perawatan (id, resort, emplasemen, noWesel, namaFile) VALUE ('$id', '$resort', '$emplasemen', '$noWesel', '$docNamaBaru')";
            $sql = mysqli_query($db, $query);

            if($sql){
                echo "
                <script>
                    alert('BERHASIL UPLOAD DOKUMEN!!');
                    document.location.href = '../detail-data-aset/?emplasemen=$emplasemen&resort=$resort&noWesel=$noWesel';
                </script>
            ";
            }else{
                echo "
                <script>
                    alert('GAGAL UPLOAD FILE KE SERVER!!');
                    document.location.href = '../detail-data-aset/?emplasemen=$emplasemen&resort=$resort&noWesel=$noWesel';
                </script>
            ";
            }

        }else{
            echo "
                <script>
                    alert('GAGAL UPLOAD FILE KE SERVER');
                    document.location.href = '../detail-data-aset/?emplasemen=$emplasemen&resort=$resort&noWesel=$noWesel';
                </script>
            ";
        }
    }else{
        echo "
                <script>
                    alert('FILE YANG ANDA UPLOAD BUKAN PDF');
                    document.location.href = '../detail-data-aset/?emplasemen=$emplasemen&resort=$resort&noWesel=$noWesel';
                </script>
            ";
    }
}
?>