<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
    header("location:../login");
    exit;
}

$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $posisiUjung = $_POST['posisiUjung'];
    $posisiPangkal = $_POST['posisiPangkal'];
    $sudutWesel = $_POST['sudutWesel'];
    $merk = $_POST['merk'];
    $arah = $_POST['arah'];
    $tipeRel = $_POST['tipeRel'];
    $lidah = $_POST['lidah'];
    $terlayan = $_POST['terlayan'];
    $tahun = $_POST['tahun'];

    if ($_FILES['fotoWesel']['tmp_name']!='') {
        $fotoWeselNama = $_FILES['fotoWesel']['name'];
        $fotoWeselTmp = $_FILES['fotoWesel']['tmp_name'];
        $fotoWeselNamaBaru = $noWesel . $fotoWeselNama;
        $fotoWeselSize = $_FILES['fotoWesel']['size'];
        $fotoWeselTipe = $_FILES['fotoWesel']['type'];

        if ($fotoWeselTipe == "image/jpeg" || $fotoWeselTipe == "image/png") {
            if ($fotoWeselSize <= 5000000) {
                if (move_uploaded_file($fotoWeselTmp, "../../src/img/" . $fotoWeselNamaBaru)) {
                    $query = "UPDATE tbl_data_aset SET posisiUjung='$posisiUjung', posisiPangkal='$posisiPangkal', sudutWesel='$sudutWesel', merk='$merk', arah='$arah', tipeRel='$tipeRel', lidah='$lidah', terlayan='$terlayan', tahun='$tahun', fotoWesel='$fotoWeselNamaBaru' WHERE id=$id";
                    $sql = mysqli_query($db, $query);

                    if ($sql) {
                        echo "
                            <script>
                                alert('DATA BERHASIL DI EDIT');
                                document.location.href = '../edit-detail-data-aset/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
                            </script>
                        ";
                    } else {
                        echo "
                            <script>
                                alert('DATA GAGAL DI EDIT');
                                document.location.href = '../edit-detail-data-aset/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
                            </script>
                        ";
                    }
                }
            } else {
                echo "
                        <script>
                            alert('DATA GAGAL DI INPUT, GAMBAR TIDAK BOLEH LEBIH DARI 5MB');
                            document.location.href = '../edit-detail-data-aset/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
                        </script>
                    ";
            }
        } else {
            echo "
                    <script>
                        alert('DATA GAGAL DI INPUT, TYPE FILE YANG ANDA UPLOAD BUKAN GAMBAR');
                        document.location.href = '../edit-detail-data-aset/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
                    </script>
                ";
        }
    }else{
        $query = "UPDATE tbl_data_aset SET posisiUjung='$posisiUjung', posisiPangkal='$posisiPangkal', sudutWesel='$sudutWesel', merk='$merk', arah='$arah', tipeRel='$tipeRel', lidah='$lidah', terlayan='$terlayan', tahun='$tahun' WHERE id=$id";
        $sql = mysqli_query($db, $query);

        if ($sql) {
            echo "
                <script>
                    alert('DATA BERHASIL DI EDIT');
                    document.location.href = '../detail-data-aset/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('DATA GAGAL DI EDIT');
                    document.location.href = '../edit-detail-data-aset/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
                </script>
            ";
        }
    }
}

?>