<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}

$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (!isset($_SESSION['id' . $noWesel])) {
	header("location:../data-aset/");
	exit;
}

if (isset($_POST['next'])){
    $id = $_SESSION['id'.$noWesel];
    $ls_ls6m_jarumLurus_nStandar = $_POST['ls_ls6m_jarumLurus_nStandar'];
    $ls_ls6m_jarumLurus_nPemeriksaan = $_POST['ls_ls6m_jarumLurus_nPemeriksaan'];
    $ls_ls6m_jarumBelok_nStandar = $_POST['ls_ls6m_jarumBelok_nStandar'];
    $ls_ls6m_jarumBelok_nPemeriksaan = $_POST['ls_ls6m_jarumBelok_nPemeriksaan'];
    $ls_lst_jarumLurus_nStandar = $_POST['ls_lst_jarumLurus_nStandar'];
    $ls_lst_jarumLurus_nPemeriksaan = $_POST['ls_lst_jarumLurus_nPemeriksaan'];
    $ls_lst_jarumBelok_nStandar = $_POST['ls_lst_jarumBelok_nStandar'];
    $ls_lst_jarumBelok_nPemeriksaan = $_POST['ls_lst_jarumBelok_nPemeriksaan'];
    $ls_la_paksaLurus_nStandar1 = $_POST['ls_la_paksaLurus_nStandar1'];
    $ls_la_paksaLurus_nPemeriksaan1 = $_POST['ls_la_paksaLurus_nPemeriksaan1'];
    $ls_la_paksaLurus_nStandar2 = $_POST['ls_la_paksaLurus_nStandar2'];
    $ls_la_paksaLurus_nPemeriksaan2 = $_POST['ls_la_paksaLurus_nPemeriksaan2'];
    $ls_la_paksaLurus_nStandar3 = $_POST['ls_la_paksaLurus_nStandar3'];
    $ls_la_paksaLurus_nPemeriksaan3 = $_POST['ls_la_paksaLurus_nPemeriksaan3'];
    $ls_la_paksaLurus_nStandar4 = $_POST['ls_la_paksaLurus_nStandar4'];
    $ls_la_paksaLurus_nPemeriksaan4 = $_POST['ls_la_paksaLurus_nPemeriksaan4'];
    $ls_la_paksaBelok_nStandar1 = $_POST['ls_la_paksaBelok_nStandar1'];
    $ls_la_paksaBelok_nPemeriksaan1 = $_POST['ls_la_paksaBelok_nPemeriksaan1'];
    $ls_la_paksaBelok_nStandar2 = $_POST['ls_la_paksaBelok_nStandar2'];
    $ls_la_paksaBelok_nPemeriksaan2 = $_POST['ls_la_paksaBelok_nPemeriksaan2'];
    $ls_la_paksaBelok_nStandar3 = $_POST['ls_la_paksaBelok_nStandar3'];
    $ls_la_paksaBelok_nPemeriksaan3 = $_POST['ls_la_paksaBelok_nPemeriksaan3'];
    $ls_la_paksaBelok_nStandar4 = $_POST['ls_la_paksaBelok_nStandar4'];
    $ls_la_paksaBelok_nPemeriksaan4 = $_POST['ls_la_paksaBelok_nPemeriksaan4'];
    $ls_ls_tMatematisLurus_nStandar = $_POST['ls_ls_tMatematisLurus_nStandar'];
    $ls_ls_tMatematisLurus_nPemeriksaan = $_POST['ls_ls_tMatematisLurus_nPemeriksaan'];
    $ls_ls_tMatematisBelok_nStandar = $_POST['ls_ls_tMatematisBelok_nStandar'];
    $ls_ls_tMatematisBelok_nPemeriksaan = $_POST['ls_ls_tMatematisBelok_nPemeriksaan'];
    $ls_ls_pLidahLurus_nStandar = $_POST['ls_ls_pLidahLurus_nStandar'];
    $ls_ls_pLidahLurus_nPemeriksaan = $_POST['ls_ls_pLidahLurus_nPemeriksaan'];
    $ls_ls_pLidahBelok_nStandar = $_POST['ls_ls_pLidahBelok_nStandar'];
    $ls_ls_pLidahBelok_nPemeriksaan = $_POST['ls_ls_pLidahBelok_nPemeriksaan'];
    $ls_ls_lantak_nStandar = $_POST['ls_ls_lantak_nStandar'];
    $ls_ls_lantak_nPemeriksaan = $_POST['ls_ls_lantak_nPemeriksaan'];
    $ls_ls_ppLidahLurus_nStandar = $_POST['ls_ls_ppLidahLurus_nStandar'];
    $ls_ls_ppLidahLurus_nPemeriksaan = $_POST['ls_ls_ppLidahLurus_nPemeriksaan'];
    $ls_ls_ppLidahBelok_nStandar = $_POST['ls_ls_ppLidahBelok_nStandar'];
    $ls_ls_ppLidahBelok_nPemeriksaan = $_POST['ls_ls_ppLidahBelok_nPemeriksaan'];

    $query = "INSERT INTO tbl_pemeriksaan_ls (
        id,
        ls_ls6m_jarumLurus_nStandar,
        ls_ls6m_jarumLurus_nPemeriksaan,
        ls_ls6m_jarumBelok_nStandar,
        ls_ls6m_jarumBelok_nPemeriksaan,
        ls_lst_jarumLurus_nStandar,
        ls_lst_jarumLurus_nPemeriksaan,
        ls_lst_jarumBelok_nStandar,
        ls_lst_jarumBelok_nPemeriksaan,
        ls_la_paksaLurus_nStandar1,
        ls_la_paksaLurus_nPemeriksaan1,
        ls_la_paksaLurus_nStandar2,
        ls_la_paksaLurus_nPemeriksaan2,
        ls_la_paksaLurus_nStandar3,
        ls_la_paksaLurus_nPemeriksaan3,
        ls_la_paksaLurus_nStandar4,
        ls_la_paksaLurus_nPemeriksaan4,
        ls_la_paksaBelok_nStandar1,
        ls_la_paksaBelok_nPemeriksaan1,
        ls_la_paksaBelok_nStandar2,
        ls_la_paksaBelok_nPemeriksaan2,
        ls_la_paksaBelok_nStandar3,
        ls_la_paksaBelok_nPemeriksaan3,
        ls_la_paksaBelok_nStandar4,
        ls_la_paksaBelok_nPemeriksaan4,
        ls_ls_tMatematisLurus_nStandar,
        ls_ls_tMatematisLurus_nPemeriksaan,
        ls_ls_tMatematisBelok_nStandar,
        ls_ls_tMatematisBelok_nPemeriksaan,
        ls_ls_pLidahLurus_nStandar,
        ls_ls_pLidahLurus_nPemeriksaan,
        ls_ls_pLidahBelok_nStandar,
        ls_ls_pLidahBelok_nPemeriksaan,
        ls_ls_lantak_nStandar,
        ls_ls_lantak_nPemeriksaan,
        ls_ls_ppLidahLurus_nStandar,
        ls_ls_ppLidahLurus_nPemeriksaan,
        ls_ls_ppLidahBelok_nStandar,
        ls_ls_ppLidahBelok_nPemeriksaan
    ) VALUE (
        '$id',
        '$ls_ls6m_jarumLurus_nStandar',
        '$ls_ls6m_jarumLurus_nPemeriksaan',
        '$ls_ls6m_jarumBelok_nStandar',
        '$ls_ls6m_jarumBelok_nPemeriksaan',
        '$ls_lst_jarumLurus_nStandar',
        '$ls_lst_jarumLurus_nPemeriksaan',
        '$ls_lst_jarumBelok_nStandar',
        '$ls_lst_jarumBelok_nPemeriksaan',
        '$ls_la_paksaLurus_nStandar1',
        '$ls_la_paksaLurus_nPemeriksaan1',
        '$ls_la_paksaLurus_nStandar2',
        '$ls_la_paksaLurus_nPemeriksaan2',
        '$ls_la_paksaLurus_nStandar3',
        '$ls_la_paksaLurus_nPemeriksaan3',
        '$ls_la_paksaLurus_nStandar4',
        '$ls_la_paksaLurus_nPemeriksaan4',
        '$ls_la_paksaBelok_nStandar1',
        '$ls_la_paksaBelok_nPemeriksaan1',
        '$ls_la_paksaBelok_nStandar2',
        '$ls_la_paksaBelok_nPemeriksaan2',
        '$ls_la_paksaBelok_nStandar3',
        '$ls_la_paksaBelok_nPemeriksaan3',
        '$ls_la_paksaBelok_nStandar4',
        '$ls_la_paksaBelok_nPemeriksaan4',
        '$ls_ls_tMatematisLurus_nStandar',
        '$ls_ls_tMatematisLurus_nPemeriksaan',
        '$ls_ls_tMatematisBelok_nStandar',
        '$ls_ls_tMatematisBelok_nPemeriksaan',
        '$ls_ls_pLidahLurus_nStandar',
        '$ls_ls_pLidahLurus_nPemeriksaan',
        '$ls_ls_pLidahBelok_nStandar',
        '$ls_ls_pLidahBelok_nPemeriksaan',
        '$ls_ls_lantak_nStandar',
        '$ls_ls_lantak_nPemeriksaan',
        '$ls_ls_ppLidahLurus_nStandar',
        '$ls_ls_ppLidahLurus_nPemeriksaan',
        '$ls_ls_ppLidahBelok_nStandar',
        '$ls_ls_ppLidahBelok_nPemeriksaan'
    )";
    $sql = mysqli_query($db, $query);
    if($sql){
        echo "
	        <script>
	            alert('DATA BERHASIL DI SIMPAN, KLIK OK UNTUK MELANJUTKAN KE HALAMAN SELANJUTNYA');
				document.location.href = '../opname/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
    }else{
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../pemeriksaan-lebar-sepur/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
    }
}

if (isset($_POST['edit'])){
    $id = $_SESSION['id'.$noWesel];
    $ls_ls6m_jarumLurus_nStandar = $_POST['ls_ls6m_jarumLurus_nStandar'];
    $ls_ls6m_jarumLurus_nPemeriksaan = $_POST['ls_ls6m_jarumLurus_nPemeriksaan'];
    $ls_ls6m_jarumBelok_nStandar = $_POST['ls_ls6m_jarumBelok_nStandar'];
    $ls_ls6m_jarumBelok_nPemeriksaan = $_POST['ls_ls6m_jarumBelok_nPemeriksaan'];
    $ls_lst_jarumLurus_nStandar = $_POST['ls_lst_jarumLurus_nStandar'];
    $ls_lst_jarumLurus_nPemeriksaan = $_POST['ls_lst_jarumLurus_nPemeriksaan'];
    $ls_lst_jarumBelok_nStandar = $_POST['ls_lst_jarumBelok_nStandar'];
    $ls_lst_jarumBelok_nPemeriksaan = $_POST['ls_lst_jarumBelok_nPemeriksaan'];
    $ls_la_paksaLurus_nStandar1 = $_POST['ls_la_paksaLurus_nStandar1'];
    $ls_la_paksaLurus_nPemeriksaan1 = $_POST['ls_la_paksaLurus_nPemeriksaan1'];
    $ls_la_paksaLurus_nStandar2 = $_POST['ls_la_paksaLurus_nStandar2'];
    $ls_la_paksaLurus_nPemeriksaan2 = $_POST['ls_la_paksaLurus_nPemeriksaan2'];
    $ls_la_paksaLurus_nStandar3 = $_POST['ls_la_paksaLurus_nStandar3'];
    $ls_la_paksaLurus_nPemeriksaan3 = $_POST['ls_la_paksaLurus_nPemeriksaan3'];
    $ls_la_paksaLurus_nStandar4 = $_POST['ls_la_paksaLurus_nStandar4'];
    $ls_la_paksaLurus_nPemeriksaan4 = $_POST['ls_la_paksaLurus_nPemeriksaan4'];
    $ls_la_paksaBelok_nStandar1 = $_POST['ls_la_paksaBelok_nStandar1'];
    $ls_la_paksaBelok_nPemeriksaan1 = $_POST['ls_la_paksaBelok_nPemeriksaan1'];
    $ls_la_paksaBelok_nStandar2 = $_POST['ls_la_paksaBelok_nStandar2'];
    $ls_la_paksaBelok_nPemeriksaan2 = $_POST['ls_la_paksaBelok_nPemeriksaan2'];
    $ls_la_paksaBelok_nStandar3 = $_POST['ls_la_paksaBelok_nStandar3'];
    $ls_la_paksaBelok_nPemeriksaan3 = $_POST['ls_la_paksaBelok_nPemeriksaan3'];
    $ls_la_paksaBelok_nStandar4 = $_POST['ls_la_paksaBelok_nStandar4'];
    $ls_la_paksaBelok_nPemeriksaan4 = $_POST['ls_la_paksaBelok_nPemeriksaan4'];
    $ls_ls_tMatematisLurus_nStandar = $_POST['ls_ls_tMatematisLurus_nStandar'];
    $ls_ls_tMatematisLurus_nPemeriksaan = $_POST['ls_ls_tMatematisLurus_nPemeriksaan'];
    $ls_ls_tMatematisBelok_nStandar = $_POST['ls_ls_tMatematisBelok_nStandar'];
    $ls_ls_tMatematisBelok_nPemeriksaan = $_POST['ls_ls_tMatematisBelok_nPemeriksaan'];
    $ls_ls_pLidahLurus_nStandar = $_POST['ls_ls_pLidahLurus_nStandar'];
    $ls_ls_pLidahLurus_nPemeriksaan = $_POST['ls_ls_pLidahLurus_nPemeriksaan'];
    $ls_ls_pLidahBelok_nStandar = $_POST['ls_ls_pLidahBelok_nStandar'];
    $ls_ls_pLidahBelok_nPemeriksaan = $_POST['ls_ls_pLidahBelok_nPemeriksaan'];
    $ls_ls_lantak_nStandar = $_POST['ls_ls_lantak_nStandar'];
    $ls_ls_lantak_nPemeriksaan = $_POST['ls_ls_lantak_nPemeriksaan'];
    $ls_ls_ppLidahLurus_nStandar = $_POST['ls_ls_ppLidahLurus_nStandar'];
    $ls_ls_ppLidahLurus_nPemeriksaan = $_POST['ls_ls_ppLidahLurus_nPemeriksaan'];
    $ls_ls_ppLidahBelok_nStandar = $_POST['ls_ls_ppLidahBelok_nStandar'];
    $ls_ls_ppLidahBelok_nPemeriksaan = $_POST['ls_ls_ppLidahBelok_nPemeriksaan'];

    $query = "UPDATE tbl_pemeriksaan_ls SET
    ls_ls6m_jarumLurus_nStandar = '$ls_ls6m_jarumLurus_nStandar',
    ls_ls6m_jarumLurus_nPemeriksaan = '$ls_ls6m_jarumLurus_nPemeriksaan',
    ls_ls6m_jarumBelok_nStandar = '$ls_ls6m_jarumBelok_nStandar',
    ls_ls6m_jarumBelok_nPemeriksaan = '$ls_ls6m_jarumBelok_nPemeriksaan',
    ls_lst_jarumLurus_nStandar = '$ls_lst_jarumLurus_nStandar',
    ls_lst_jarumLurus_nPemeriksaan = '$ls_lst_jarumLurus_nPemeriksaan',
    ls_lst_jarumBelok_nStandar = '$ls_lst_jarumBelok_nStandar',
    ls_lst_jarumBelok_nPemeriksaan = '$ls_lst_jarumBelok_nPemeriksaan',
    ls_la_paksaLurus_nStandar1 = '$ls_la_paksaLurus_nStandar1',
    ls_la_paksaLurus_nPemeriksaan1 = '$ls_la_paksaLurus_nPemeriksaan1',
    ls_la_paksaLurus_nStandar2 = '$ls_la_paksaLurus_nStandar2',
    ls_la_paksaLurus_nPemeriksaan2 = '$ls_la_paksaLurus_nPemeriksaan2',
    ls_la_paksaLurus_nStandar3 = '$ls_la_paksaLurus_nStandar3',
    ls_la_paksaLurus_nPemeriksaan3 = '$ls_la_paksaLurus_nPemeriksaan3',
    ls_la_paksaLurus_nStandar4 = '$ls_la_paksaLurus_nStandar4',
    ls_la_paksaLurus_nPemeriksaan4 = '$ls_la_paksaLurus_nPemeriksaan4',
    ls_la_paksaBelok_nStandar1 = '$ls_la_paksaBelok_nStandar1',
    ls_la_paksaBelok_nPemeriksaan1 = '$ls_la_paksaBelok_nPemeriksaan1',
    ls_la_paksaBelok_nStandar2 = '$ls_la_paksaBelok_nStandar2',
    ls_la_paksaBelok_nPemeriksaan2 = '$ls_la_paksaBelok_nPemeriksaan2',
    ls_la_paksaBelok_nStandar3 = '$ls_la_paksaBelok_nStandar3',
    ls_la_paksaBelok_nPemeriksaan3 = '$ls_la_paksaBelok_nPemeriksaan3',
    ls_la_paksaBelok_nStandar4 = '$ls_la_paksaBelok_nStandar4',
    ls_la_paksaBelok_nPemeriksaan4 = '$ls_la_paksaBelok_nPemeriksaan4',
    ls_ls_tMatematisLurus_nStandar = '$ls_ls_tMatematisLurus_nStandar',
    ls_ls_tMatematisLurus_nPemeriksaan = '$ls_ls_tMatematisLurus_nPemeriksaan',
    ls_ls_tMatematisBelok_nStandar = '$ls_ls_tMatematisBelok_nStandar',
    ls_ls_tMatematisBelok_nPemeriksaan = '$ls_ls_tMatematisBelok_nPemeriksaan',
    ls_ls_pLidahLurus_nStandar = '$ls_ls_pLidahLurus_nStandar',
    ls_ls_pLidahLurus_nPemeriksaan = '$ls_ls_pLidahLurus_nPemeriksaan',
    ls_ls_pLidahBelok_nStandar = '$ls_ls_pLidahBelok_nStandar',
    ls_ls_pLidahBelok_nPemeriksaan = '$ls_ls_pLidahBelok_nPemeriksaan',
    ls_ls_lantak_nStandar = '$ls_ls_lantak_nStandar',
    ls_ls_lantak_nPemeriksaan = '$ls_ls_lantak_nPemeriksaan',
    ls_ls_ppLidahLurus_nStandar = '$ls_ls_ppLidahLurus_nStandar',
    ls_ls_ppLidahLurus_nPemeriksaan = '$ls_ls_ppLidahLurus_nPemeriksaan',
    ls_ls_ppLidahBelok_nStandar = '$ls_ls_ppLidahBelok_nStandar',
    ls_ls_ppLidahBelok_nPemeriksaan = '$ls_ls_ppLidahBelok_nPemeriksaan' WHERE id='$id'
    ";
    $sql = mysqli_query($db, $query);
    if($sql){
        echo "
	        <script>
	            alert('DATA BERHASIL DI SIMPAN, KLIK OK UNTUK MELANJUTKAN KE HALAMAN SELANJUTNYA');
				document.location.href = '../opname/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
    }else{
        echo "
	        <script>
	            alert('DATA GAGAL DI INPUT');
				document.location.href = '../pemeriksaan-lebar-sepur/?emplasemen=".$emplasemen."&resort=".$resort."&noWesel=".$noWesel."';
	        </script>
	        ";
    }
}