<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (!isset($_SESSION['id' . $noWesel])) {
	header("location:../data-aset/");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/opname-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Pemeriksaan Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
			$sqlWesel = mysqli_query($db, $queryWesel);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">WESEL <?php echo $noWesel ?>-Pemeriksaan Wesel</p>
			<?php
			} else {
			?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>

		<?php
		$queryCheck = "SELECT * FROM `tbl_pemeriksaan_op` WHERE id='" . $_SESSION['id' . $noWesel] . "'";
		$sqlCheck = mysqli_query($db, $queryCheck);
		if (mysqli_num_rows($sqlCheck) == 0) {
		?>
			<div class="judul-form">
				<p>Opname Lain-lain</p>
			</div>
			<form action="confOpname.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" enctype="multipart/form-data" method="POST">
				<div class="pemeriksaan">
					<div class="card w-100 mb-4">
						<div class="card-body">
							<div class="row g-3">
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Kondisi Jarum</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Kondisi Jarum" name="op_kondisiJarum" required>
								</div>
								<div class="col-md-8">
									<label for="inputEmail4" class="form-label">Foto Kondisi Jarum ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="op_kondisiJarum_foto" required>
								</div>

								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Kondisi Vangrel</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Kondisi Vangrel" name="op_kondisiVangrel" required>
								</div>
								<div class="col-md-8">
									<label for="inputEmail4" class="form-label">Foto Kondisi Vangrel ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="op_kondisiVangrel_foto" required>
								</div>

								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Kondisi Lidah</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Kondisi Lidah" name="op_kondisiLidah" required>
								</div>
								<div class="col-md-8">
									<label for="inputEmail4" class="form-label">Foto Kondisi Lidah ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="op_kondisiLidah_foto" required>
								</div>

								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Kondisi Rel Lantak</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Kondisi Rel Lantak" name="op_kondisiRelLantak" required>
								</div>
								<div class="col-md-8">
									<label for="inputEmail4" class="form-label">Foto Kondisi Rel Lantak ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="op_kondisiRelLantak_foto" required>
								</div>

								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Kondisi Bantalan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Kondisi Bantalan" name="op_kondisiBantalan" required>
								</div>
								<div class="col-md-8">
									<label for="inputEmail4" class="form-label">Foto Kondisi Bantalan ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="op_kondisiBantalan_foto" required>
								</div>

								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Kondisi Penambat</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Kondisi Penambat" name="op_kondisiPenambat" required>
								</div>
								<div class="col-md-8">
									<label for="inputEmail4" class="form-label">Foto Kondisi Penambat ukuran max 5MB</label>
									<input class="form-control" type="file" id="fotoWesel" name="op_kondisiPenambat_foto" required>
								</div>

								<p style="margin-bottom: 0px;">Mengetahui</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nama KUPT</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nama KUPT" name="nama_kupt" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">NIPP</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan NIPP" name="nipp_kupt" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">No Telepon/WhatsApp</label>
									<input type="tel" class="form-control" id="inputEmail4" placeholder="Masukkan No Telepon" name="noHp_kupt" required>
								</div>

								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nama KAUR</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nama KAUR" name="nama_kat" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">NIPP</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan NIPP" name="nipp_kat" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">No Telepon/WhatsApp</label>
									<input type="tel" class="form-control" id="inputEmail4" placeholder="Masukkan No Telepon" name="noHp_kat" required>
								</div>
							</div>
						</div>
					</div>
					<div class="tombol d-flex justify-content-between mb-4">
						<button type='button' class='btn btn-primary btn-sm' data-bs-toggle='modal' data-bs-target='#kembali'>Halaman Sebelumnya</button>
						<button type='button' class='btn btn-primary btn-sm' data-bs-toggle='modal' data-bs-target='#simpanData'>Simpan</button>
					</div>
					<div class='modal fade' id='simpanData' tabindex='-1' aria-labelledby='simpanDataLabel' aria-hidden='true'>
						<div class='modal-dialog'>
							<div class='modal-content'>
								<div class='modal-header'>
									<h5 class='modal-title' id='simpanDataLabel'>Peringatan!!</h5>
									<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
								</div>
								<div class='modal-body'>
									Apakah anda yakin untuk menyelesaikan form ini? setelah anda klik simpan maka data tidak dapat di rubah lagi
								</div>
								<div class='modal-footer'>
									<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
									<button type='submit' class='btn btn-primary' value='save' name='save'>Simpan</button>
								</div>
							</div>
						</div>
					</div>
					<div class='modal fade' id='kembali' tabindex='-1' aria-labelledby='kembaliLabel' aria-hidden='true'>
						<div class='modal-dialog'>
							<div class='modal-content'>
								<div class='modal-header'>
									<h5 class='modal-title' id='kembaliLabel'>Peringatan!!</h5>
									<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
								</div>
								<div class='modal-body'>
									Apakah anda yakin untuk kembali ke halaman sebelumnya? progress anda di halaman ini tidak akan tersimpan
								</div>
								<div class='modal-footer'>
									<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Batal</button>
									<a class="btn btn-primary" href="../pemeriksaan-lebar-sepur/?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" role="button">Halaman Sebelumnya</a>
								</div>
							</div>
						</div>
					</div>
			</form>
		<?php
		}
		?>


	</div>
	<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>