<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../login");
	exit;
}
$emplasemen = $_GET['emplasemen'];
$resort = $_GET['resort'];
$noWesel = $_GET['noWesel'];

if (!isset($_SESSION['id' . $noWesel])) {
	$_SESSION['id' . $noWesel] = "";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/pemeriksaan-admin.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Pemeriksaan Wesel - Admin</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<img src="../../src/image/kaiLogo.png" alt="">
		</div>
		<a href="../"><img src="../../src/icon/icon-home.png" alt="" class="icon">Home</a>
		<a href="../data-aset" class="active"><img src="../../src/icon/icon-resort.png" alt="" class="icon">Data Aset</a>
		<a href="../arsip/"><img src="../../src/icon/icon-arsip.png" alt="" class="icon">Arsip</a>
		<a href="../status-reporting/"><img src="../../src/icon/icon-reporting.png" alt="" class="icon">Status Reporting</a>
		<a href="../about/"><img src="../../src/icon/icon-about.png" alt="" class="icon">About</a>
		<a href="../logout.php"><img src="../../src/icon/icon-logout.png" alt="" class="icon">Logout</a>
	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<div class="nama text-end">
			<p>SELL <span>Maintenance</span></p>
		</div>

		<div class="judul text-center">
			<?php
			$queryResort = "SELECT * FROM `tbl_resort` WHERE namaResort='$resort'";
			$sqlResort = mysqli_query($db, $queryResort);
			$queryEmplasemen = "SELECT * FROM `tbl_emplasemen` WHERE namaEmplasemen='$emplasemen'";
			$sqlEmplasemen = mysqli_query($db, $queryEmplasemen);
			$queryWesel = "SELECT * FROM `tbl_data_aset` WHERE noWesel='$noWesel'";
			$sqlWesel = mysqli_query($db, $queryWesel);
			if (mysqli_num_rows($sqlResort) != 0 && mysqli_num_rows($sqlEmplasemen) != 0 && mysqli_num_rows($sqlWesel) != 0) {
			?>
				<p class="resor"><?php echo $resort ?></p>
				<p class="emplasemen"><?php echo $emplasemen ?></p>
				<p class="data-aset-wesel">WESEL <?php echo $noWesel ?>-Pemeriksaan Wesel</p>
			<?php
			} else {
			?>
				<p class="resor">DATA TIDAK DI TEMUKAN</p>
				<p class="emplasemen">HARAP BERITAHUKAN SUPER ADMIN UNTUK DATA TERSEBUT</p>
				<p class="data-aset-wesel">ATAU JANGAN MENGUBAH DOMAIN SECARA MANUAL</p>
			<?php
			}
			?>
		</div>

		<?php
		$queryCheck = "SELECT * FROM `tbl_pemeriksaan` WHERE noWesel='$noWesel' AND `status`='belumSelesai'";
		$sqlCheck = mysqli_query($db, $queryCheck);
		$queryCheck2 = "SELECT * FROM tbl_pemeriksaan WHERE noWesel='$noWesel' AND `status`='selesaiPemeriksaan'";
		$sqlCheck2 = mysqli_query($db,$queryCheck2);
		if(mysqli_num_rows($sqlCheck2) != 0){
			echo "ANDA PERLU MENYELESAIKAN PERAWATAN TERLEBIH DULU UNTUK MELAKUKAN PEMERIKSAAN KEMBALI"; 
			// DISINI GANTI JADI TAMPILAN HTML YANG RAPI YA DO
		}else  if ($_SESSION['id' . $noWesel] == "" && mysqli_num_rows($sqlCheck) == 0) {
		?>
			<form action="confPemeriksaan.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" method="POST">
				<div class="judul-form d-flex justify-content-between">
					<p>Ukuran Tempat-tempat Penting</p>
					<div class="tanggal col-md-3">
						<input type="hidden" class="form-control" id="exampleFormControlInput1" name="tanggal" value="<?php echo date('y-m-d') ?>" required>
					</div>
				</div>

				<div class="pemeriksaan">
					<div class="card w-100 mb-4">
						<div class="card-body">
							<div class="row g-3">
								<p style="margin-bottom: 0px;">Lebar Alur pada Jarum dengan Rel Paksa Lurus</p>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_laj_paksaLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_laj_paksaLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_laj_paksaLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Alur pada Jarum dengan Rel Paksa Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_laj_paksaBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_laj_paksaBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_laj_paksaBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Lebar Alur pada Rel Paksa Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_la_paksaLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_la_paksaLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_la_paksaLurus_nPerawatan" disabled>
								</div>
								<p style="margin-bottom: 0px;">Lebar Alur pada Rel Paksa Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_la_paksaBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_la_paksaBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_la_paksaBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Jarak Antara Pangkal Lidah dan Rel Lantak Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_japl_lantakLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_japl_lantakLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_japl_lantakLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Jarak Antara Pangkal Lidah dan Rel Lantak Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_japl_lantakBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_japl_lantakBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_japl_lantakBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Connecting Rod</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_connectingRod_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_connectingRod_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_connectingRod_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Jarak Antara Ujung Lidah Terbuka dengan Rel Lantak Lurus</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_jault_lantakLurus_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_jault_lantakLurus_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_jault_lantakLurus_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Jarak Antara Ujung Lidah Terbuka dengan Rel Lantak Belok</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_jault_lantakBelok_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_jault_lantakBelok_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_jault_lantakBelok_nPerawatan" disabled>
								</div>

								<p style="margin-bottom: 0px;">Tidak Sikunya Sambungan pada Rel Lantak</p>
								<div class="col-md-4">
									<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
									<label for="inputEmail4" class="form-label">Nilai Standar</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_tss_lantak_nStandar" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_tss_lantak_nPemeriksaan" required>
								</div>
								<div class="col-md-4">
									<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
									<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_tss_lantak_nPerawatan" disabled>
								</div>
							</div>
						</div>
					</div>
					<div class="tombol d-flex justify-content-end mb-4">
						<button class="btn btn-primary btn-sm" type="submit" value="next" name="next">Halaman Selanjutnya</button>
					</div>
				</div>
			</form>
			<?php
		} else if ($_SESSION['id' . $noWesel] != "" && mysqli_num_rows($sqlCheck) != 0) {
			$id = $_SESSION['id' . $noWesel];
			$queryPemeriksaan = "SELECT * FROM `tbl_pemeriksaan` WHERE id='$id'";
			$sqlPemeriksaan = mysqli_query($db, $queryPemeriksaan);
			$queryPemeriksaanUkt = "SELECT * FROM `tbl_pemeriksaan_ukt` WHERE id='$id'";
			$sqlPemeriksaanUkt = mysqli_query($db, $queryPemeriksaanUkt);
			if ($dataPemeriksaan = mysqli_fetch_array($sqlPemeriksaan) && $dataPemeriksaanUkt = mysqli_fetch_array($sqlPemeriksaanUkt)) {
			?>
				<form action="confPemeriksaan.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" method="POST">
					<div class="judul-form d-flex justify-content-between">
						<p>Ukuran Tempat-tempat Penting</p>
					</div>

					<div class="pemeriksaan">
						<div class="card w-100 mb-4">
							<div class="card-body">
								<div class="row g-3">
									<p style="margin-bottom: 0px;">Lebar Alur pada Jarum dengan Rel Paksa Lurus</p>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_laj_paksaLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_laj_paksaLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_laj_paksaLurus_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Lebar Alur pada Jarum dengan Rel Paksa Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_laj_paksaBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_laj_paksaBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_laj_paksaBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Lebar Alur pada Rel Paksa Lurus</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_la_paksaLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_la_paksaLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_la_paksaLurus_nPerawatan" disabled>
									</div>
									<p style="margin-bottom: 0px;">Lebar Alur pada Rel Paksa Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_la_paksaBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_la_paksaBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_la_paksaBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Pangkal Lidah dan Rel Lantak Lurus</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_japl_lantakLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_japl_lantakLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_japl_lantakLurus_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Pangkal Lidah dan Rel Lantak Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_japl_lantakBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_japl_lantakBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_japl_lantakBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Connecting Rod</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_connectingRod_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_connectingRod_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_connectingRod_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_connectingRod_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_connectingRod_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Ujung Lidah Terbuka dengan Rel Lantak Lurus</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_jault_lantakLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_jault_lantakLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_jault_lantakLurus_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Ujung Lidah Terbuka dengan Rel Lantak Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_jault_lantakBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_jault_lantakBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_jault_lantakBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Tidak Sikunya Sambungan pada Rel Lantak</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_tss_lantak_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_tss_lantak_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_tss_lantak_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_tss_lantak_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_tss_lantak_nPerawatan" disabled>
									</div>
								</div>
							</div>
						</div>
						<div class="tombol d-flex justify-content-end mb-4">
							<button class="btn btn-primary btn-sm" type="submit" value="edit" name="edit">Halaman Selanjutnya</button>
						</div>
					</div>
				</form>
			<?php
			}
		} else if (mysqli_num_rows($sqlCheck) != 0 && $_SESSION['id' . $noWesel] == "") {
			if ($cariId = mysqli_fetch_array($sqlCheck)) {
				$id = $cariId['id'];
				$_SESSION['id' . $noWesel] = $id;
			}
			$queryPemeriksaan = "SELECT * FROM `tbl_pemeriksaan` WHERE id='$id'";
			$sqlPemeriksaan = mysqli_query($db, $queryPemeriksaan);
			$queryPemeriksaanUkt = "SELECT * FROM `tbl_pemeriksaan_ukt` WHERE id='$id'";
			$sqlPemeriksaanUkt = mysqli_query($db, $queryPemeriksaanUkt);
			if ($dataPemeriksaan = mysqli_fetch_array($sqlPemeriksaan) && $dataPemeriksaanUkt = mysqli_fetch_array($sqlPemeriksaanUkt)) {
			?>
				<form action="confPemeriksaan.php?emplasemen=<?php echo $emplasemen ?>&resort=<?php echo $resort ?>&noWesel=<?php echo $noWesel ?>" method="POST">
					<div class="judul-form d-flex justify-content-between">
						<p>Ukuran Tempat-tempat Penting</p>
					</div>

					<div class="pemeriksaan">
						<div class="card w-100 mb-4">
							<div class="card-body">
								<div class="row g-3">
									<p style="margin-bottom: 0px;">Lebar Alur pada Jarum dengan Rel Paksa Lurus</p>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_laj_paksaLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_laj_paksaLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_laj_paksaLurus_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Lebar Alur pada Jarum dengan Rel Paksa Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_laj_paksaBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_laj_paksaBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_laj_paksaBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_laj_paksaBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Lebar Alur pada Rel Paksa Lurus</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_la_paksaLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_la_paksaLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_la_paksaLurus_nPerawatan" disabled>
									</div>
									<p style="margin-bottom: 0px;">Lebar Alur pada Rel Paksa Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_la_paksaBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_la_paksaBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_la_paksaBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_la_paksaBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Pangkal Lidah dan Rel Lantak Lurus</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_japl_lantakLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_japl_lantakLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_japl_lantakLurus_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Pangkal Lidah dan Rel Lantak Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_japl_lantakBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_japl_lantakBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_japl_lantakBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_japl_lantakBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Connecting Rod</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_connectingRod_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_connectingRod_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_connectingRod_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_connectingRod_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_connectingRod_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Ujung Lidah Terbuka dengan Rel Lantak Lurus</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_jault_lantakLurus_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakLurus_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_jault_lantakLurus_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakLurus_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_jault_lantakLurus_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Jarak Antara Ujung Lidah Terbuka dengan Rel Lantak Belok</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_jault_lantakBelok_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakBelok_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_jault_lantakBelok_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_jault_lantakBelok_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_jault_lantakBelok_nPerawatan" disabled>
									</div>

									<p style="margin-bottom: 0px;">Tidak Sikunya Sambungan pada Rel Lantak</p>
									<div class="col-md-4">
										<!-- <label for="inputEmail4" class="form-label">Lebar Alur pada Jarum dengan Rel Paksa Lurus</label><br> -->
										<label for="inputEmail4" class="form-label">Nilai Standar</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Standar" name="ukt_tss_lantak_nStandar" value="<?php echo $dataPemeriksaanUkt['ukt_tss_lantak_nStandar'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Pemeriksaan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Pemeriksaan" name="ukt_tss_lantak_nPemeriksaan" value="<?php echo $dataPemeriksaanUkt['ukt_tss_lantak_nPemeriksaan'] ?>" required>
									</div>
									<div class="col-md-4">
										<label for="inputEmail4" class="form-label">Nilai Perawatan</label>
										<input type="text" class="form-control" id="inputEmail4" placeholder="Masukkan Nilai Perawatan" name="ukt_tss_lantak_nPerawatan" disabled>
									</div>
								</div>
							</div>
						</div>
						<div class="tombol d-flex justify-content-end mb-4">
							<button class="btn btn-primary btn-sm" type="submit" value="edit" name="edit">Halaman Selanjutnya</button>
						</div>
					</div>
				</form>
		<?php
			}
		}
		?>
		<!-- end content -->
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>